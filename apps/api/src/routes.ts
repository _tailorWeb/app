import { Routes } from 'nest-router'
import { SymbolsModule } from '@simulator/symbols'
import { ApiProfileModule } from '@simulator/api/profile'
import { ApiDhxModule } from '@simulator/api/dhx'
import { ApiMxcModule } from '@simulator/api/mxc'
import { ApiDevicesModule } from '@simulator/api/devices'

export const routes: Routes = [
  {
    path: 'v1',
    children: [
      {
        path: 'symbols',
        module: SymbolsModule,
      },
      {
        path: 'profile',
        module: ApiProfileModule,
      },
      {
        path: 'dhx',
        module: ApiDhxModule,
      },
      {
        path: 'mxc',
        module: ApiMxcModule,
      },
      {
        path: 'device',
        module: ApiDevicesModule,
      },
    ],
  },
]
