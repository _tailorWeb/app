import { Logger } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app/app.module'
import * as helmet from 'fastify-helmet'
import compression from 'fastify-compress'
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import fastifyCookie from 'fastify-cookie'
import { AppConfig, appConfiguration } from '@simulator/configuration'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter(),  { cors: true })

  const appConfig = app.get<AppConfig>(appConfiguration.KEY)

  await app.register(compression, { encodings: ['gzip', 'deflate'] })

  await app.register(helmet.fastifyHelmet, {
    contentSecurityPolicy: {
      directives: {
        defaultSrc: [`'self'`],
        styleSrc: [`'self'`, `'unsafe-inline'`],
        imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
        scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
      },
    },
  })

  await app.register(fastifyCookie, {
    secret: 'Tszk3GlJpu', // for cookies signature
  })

  await app.register(require('fastify-rate-limit'), {
    max: 100,
    timeWindow: '1 minute',
  })

  const globalPrefix = 'api'

  app.setGlobalPrefix(globalPrefix)

  const swaggerDocOptions = new DocumentBuilder()
    .setTitle('Simulator API')
    .setDescription('API documentation for Simulator MXC')
    .setVersion('1.0.0')
    // .addServer(`${appConfig.domain}`, 'Development API')
    .addBearerAuth({
      name: 'Authorization',
      in: 'header',
      type: 'apiKey',
    })
    .build()
  const swaggerDoc = SwaggerModule.createDocument(app, swaggerDocOptions)
  SwaggerModule.setup('api', app, swaggerDoc, {
    swaggerOptions: {
      docExpansion: 'none',
      filter: true,
      showRequestDuration: true,
    },
  })

  await app.listen(appConfig.port, '0.0.0.0')
}

bootstrap()
