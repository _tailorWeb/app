import { Module } from '@nestjs/common'

import { AppController } from './app.controller'
import { ApiConfigModule } from '@simulator/config'
import { RouterModule } from 'nest-router'
import { routes } from '../routes'
import { SymbolsModule } from '@simulator/symbols'
import { ApiProfileModule } from '@simulator/api/profile'
import { ApiDhxModule } from '@simulator/api/dhx'
import { ApiMxcModule } from '@simulator/api/mxc'
import { ApiAuthModule } from '@simulator/api/auth'
import { ApiDevicesModule } from '@simulator/api/devices'

@Module({
  imports: [
    ApiConfigModule,
    RouterModule.forRoutes(routes),
    ApiAuthModule,
    SymbolsModule,
    ApiProfileModule,
    ApiDhxModule,
    ApiMxcModule,
    ApiDevicesModule
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
