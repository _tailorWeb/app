import { Component } from '@angular/core'

@Component({
  selector: 'simulator-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}
