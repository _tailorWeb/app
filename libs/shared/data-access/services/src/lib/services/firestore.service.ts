import {Injectable} from '@angular/core';
import {map, take, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {
    AngularFirestore,
    AngularFirestoreCollection,
    AngularFirestoreCollectionGroup,
    AngularFirestoreDocument, DocumentReference,
} from '@angular/fire/firestore'
// Custom Types
type CollectionPredicate<T> = string | AngularFirestoreCollection<T>;
type DocPredicate<T> = string | AngularFirestoreDocument<T>;

interface ItemId {
    id?: string;
}

@Injectable({
    providedIn: 'root'
})
export class FirestoreService {

    constructor(private afs: AngularFirestore) {
    }

    col<T>(ref: CollectionPredicate<T>, queryFn?): AngularFirestoreCollection<T> {
        return typeof ref === 'string' ? this.afs.collection<T>(ref, queryFn) : ref;
    }

    doc<T>(ref: DocPredicate<T>): AngularFirestoreDocument<T> {
        return typeof ref === 'string' ? this.afs.doc<T>(ref) : ref;
    }

    /**
     * USAGE: this.db.doc$('notes/ID')
     */
    doc$<T extends ItemId>(ref: DocPredicate<T>): Observable<T> {
        return this.doc(ref).snapshotChanges().pipe(
            map(doc => {
                // console.log('document fromCache:', doc.payload.metadata.fromCache);
                if (doc.payload.exists) {
                    const data = doc.payload.data() || {};
                    const id = doc.payload.id;
                    return {id, ...data} as T;
                } else {
                    return null as T;
                }
            }));
    }


    /**
     * USAGE: this.db.col$('notes', ref => ref.where('user', '==', 'Jeff'))
     */
    col$<T extends ItemId>(ref: CollectionPredicate<T>, queryFn?): Observable<T[]> {
        return this.col(ref, queryFn).snapshotChanges().pipe(
            map(docs => {
                return docs.map(a => {
                    // console.log('collection fromCache:', a.payload.doc.metadata.fromCache);
                    const data = a.payload.doc.data() || {};
                    const id = a.payload.doc.id;
                    return {id, ...data};
                }) as T[];
            }));
    }

    // time since the Unix epoch, in milliseconds
    get timestamp() {
        return Date.now();
    }

    /**
     * USAGE: this.db.update('items/ID', data) }) // adds updatedAt field
     */
    update<T>(ref: DocPredicate<T>, data: any): Promise<void> {
        return this.doc(ref).update({
            ...data,
            updatedAt: this.timestamp
        });
    }

    /**
     * USAGE: this.db.set('items/ID', data) })    // adds createdAt field
     */
    set<T>(ref: DocPredicate<T>, data: any): Promise<any> {
        const timestamp = this.timestamp;
        return this.doc(ref).set({
            ...data,
            updatedAt: timestamp
        }, {merge: true});
    }

    /**
     * USAGE: this.db.remove('items/ID') })
     */
    remove<T>(ref: DocPredicate<T>): Promise<any> {
        return this.doc(ref).delete();
    }


    /**
     * USAGE: this.db.add('items', data) })       // adds createdAt field
     */
    add<T>(ref: CollectionPredicate<T>, data): Promise<DocumentReference> {
        const timestamp = this.timestamp;
        return this.col(ref).add({
            ...data,
            updatedAt: timestamp,
            createdAt: timestamp
        });
    }

    /**
     * USAGE: this.db.upsert('notes/xyz', { content: 'hello dude'})
     * NOTE: We could of use db.set(data, { merge: true }) but this makes it difficult to automatically manage the timestamps
     */
    async upsert<T>(ref: DocPredicate<T>, data: any): Promise<void> {
        // Check if the document exists
        const doc = await this.doc(ref).snapshotChanges().pipe(take(1)).toPromise();
        // Update or Insert
        return doc.payload.exists ? this.update(ref, data) : this.set(ref, data);
    }

    /**
     * USAGE: this.db.inspectDoc('notes/xyz')
     */
    inspectDoc(ref: DocPredicate<any>): void {
        const tick = new Date().getTime();
        this.doc(ref).snapshotChanges()
            .pipe(
                take(1),
                tap(d => {
                    const tock = new Date().getTime() - tick;
                    console.log(`Loaded Document in ${tock}ms`, d);
                }))
            .subscribe();
    }


    /// **************
    /// Create and read doc references
    /// **************
    /// create a reference between two documents
    connect(host: DocPredicate<any>, key: string, doc: DocPredicate<any>) {
        return this.doc(host).update({[key]: this.doc(doc).ref});
    }


}
