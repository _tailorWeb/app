import { InjectionToken, ValueProvider } from '@angular/core'
import { FirebaseAppConfig } from '@angular/fire'

export interface AppConfig {
  production: boolean;
  baseURL: string;
  firebaseConfig: FirebaseAppConfig;
}

export const APP_CONFIG = new InjectionToken<AppConfig>('mxc-api.config')

export const getAppConfigProvider = (value: AppConfig): ValueProvider => ({
  provide: APP_CONFIG,
  useValue: value,
})
