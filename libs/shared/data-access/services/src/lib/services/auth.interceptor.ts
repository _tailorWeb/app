import { Injectable, Provider } from '@angular/core'
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { AuthService } from './auth.service'
import { catchError, mergeMap, take } from 'rxjs/operators'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('auth')) {
      return next.handle(req)
    }
    return this.authService.getToken()
      .pipe(
        take(1),
        mergeMap((token) => {
          if (token) {
            const cloned = req.clone({
              headers: req.headers.set('Authorization',
                'Bearer ' + token),
            })

            return next.handle(cloned).pipe(
              catchError((error: HttpErrorResponse) => {
                let errorMessage = ''
                if (error.error instanceof ErrorEvent) {
                  // client-side error
                  errorMessage = `Error: ${error.error.message}`
                } else {
                  // server-side error
                  errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`
                }
                console.log('err', errorMessage)
                return throwError(errorMessage)
              }),
            )
          } else {
            return next.handle(req)
          }
        }),
      )
  }
}

export const authInterceptorProvider: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
};
