import {Injectable} from '@angular/core';
import {Router, CanLoad, Route, UrlSegment, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {map, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {
  constructor(private auth: AuthService, private router: Router) {
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkAuth();
  }

  canActivate() {
    return this.checkAuth();
  }

  checkAuth() {
    return this.auth.getAuth().pipe(
      take(1),
      map(user => {
        if (!user) {
          this.router.navigate(['/auth/sign-in']);
          return false;
        }
        return true;
      })
    );
  }
}
