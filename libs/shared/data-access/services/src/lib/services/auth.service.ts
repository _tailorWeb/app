import { Inject, Injectable } from '@angular/core'
import { BehaviorSubject, from, Observable, of } from 'rxjs'
import { catchError, map, switchMap, take } from 'rxjs/operators'
import { AngularFireAuth } from '@angular/fire/auth'
import { FirestoreService } from './firestore.service'
import { AngularFirestoreDocument } from '@angular/fire/firestore'
import firebase from 'firebase/app'
import { UserDto } from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from './app-config.token'
import { HttpClient } from '@angular/common/http'
import { AngularFireAnalytics } from '@angular/fire/analytics'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: BehaviorSubject<UserDto | null> = new BehaviorSubject<| null>(null)
  user$: Observable<UserDto | null> = this.user.asObservable()

  constructor(private readonly afAuth: AngularFireAuth,
              private db: FirestoreService,
              @Inject(APP_CONFIG) private appConfig: AppConfig,
              private readonly analytics: AngularFireAnalytics,
              private httpClient: HttpClient) {
    this.getAuth()
      .pipe(
        switchMap(user => {
          if (user) {
            return this.db.doc$(`users/${user.uid}`).pipe(
              take(1),
              map((userDoc: UserDto) => {
                this.user.next(userDoc)
                this.analytics.setUserId(user.uid).catch(console.log)
                return userDoc
              }),
              catchError(() => {
                this.user.next(null)
                return of(null)
              }))
          } else {
            this.user.next(null)
            return of(null)
          }
        })).subscribe()
  }

  getAuth() {
    return this.afAuth.authState
  }

  getUser() {
    return this.user$
  }

  getToken(): Observable<string> {
    return from(firebase.auth().currentUser.getIdToken())
  }

  /**
   * Login with Credentials
   */
  login(credentials: { email: string, password: string }) {
    return this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        this.analytics.logEvent('login', { method: 'Email' })
      })
  }

  /* Logout */
  logout() {
    return this.afAuth.signOut()
  }

  /* Register a new user */
  async register({ email, password }) {
    const user = await this.afAuth.createUserWithEmailAndPassword(email, password)
    return this.updateUserData(user.user, 'Email')
  }

  /**
   * Login with Social Providers
   */
  async signInProvider(provider: 'google' | 'facebook' | 'twitter') {
    switch (provider) {
      case 'google':
        return this.signInPopup(new firebase.auth.GoogleAuthProvider())
      case 'facebook':
        return this.signInPopup(new firebase.auth.FacebookAuthProvider())
      case 'twitter':
        return this.signInPopup(new firebase.auth.TwitterAuthProvider())
    }
  }

  private async signInPopup(provider) {
    const credential = await this.afAuth.signInWithPopup(provider)
    return this.updateUserData(credential.user, provider)
  }

  private async updateUserData(user, provider) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<UserDto> = this.db.doc(`users/${user.uid}`)
    /* If the user already present */
    if (!(await userRef.get().pipe(take(1)).toPromise()).exists) {
      return this.analytics.logEvent('login', { method: provider })
    }

    const data = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
    }

    return userRef.set(data, { merge: true }).then(() => this.analytics.logEvent('sign_up', { method: provider }))

  }

  /**
   * Reset Password
   */
  resetPwd(email: string) {
    return this.afAuth.sendPasswordResetEmail(email)
  }

  /**
   * Sign Out
   */
  signOut() {
    return this.afAuth.signOut()
  }

  /* Sync with DataDash App */
  sync({ username, password, api }) {
    return this.httpClient
      .post(`${this.appConfig.baseURL}/profile/synchronize`, { username, password, api }).toPromise()
  }

  /* Send Suggestion */
  sendSuggestion(message: string) {
    return this.httpClient
      .post(`${this.appConfig.baseURL}/profile/suggestion`, { message }).toPromise()
  }
}
