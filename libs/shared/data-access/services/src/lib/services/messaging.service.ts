import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {of, Subject} from 'rxjs';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {
  catchError,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import {FirestoreService} from './firestore.service';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  token;
  private messageSource = new Subject();
  currentMessage = this.messageSource.asObservable(); // message observable to show in Angular component

  constructor(
    private afMessaging: AngularFireMessaging,
    private db: FirestoreService
  ) {
  }

  // get permission to send messages
  requestToken(user): Observable<any> {
    return this.afMessaging.requestToken.pipe(
      tap(
        token => {
          this.token = token;
          this.saveToken(user, token).catch(console.log);
        },
        err => {
          console.error('Unable to get permission to notify.', err);
        }
      )
    );
  }

  getPermission(user) {
    return this.afMessaging.requestPermission.pipe(
      switchMap(() => {
        return this.requestToken(user);
      }),
      catchError(err => {
        return of(err);
      })
    );
  }

  // save the permission token in firestore
  private async saveToken(user, token) {
    const currentTokens = await this.db
      .col$(`devices`, ref => ref.where('userId', '==', user.id))
      .pipe(take(1))
      .toPromise();
    // If token does not exist in firestore, update db
    let isTokenPresent = false;
    currentTokens.map((el: any) => {
      if (el.id === token) {
        isTokenPresent = true;
      }
    });
    if (!isTokenPresent) {
      await this.db.set(`devices/${token}`, {
        token,
        userId: user.id
      });
    }
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessages() {
    this.afMessaging.messages.subscribe(payload => {
      console.log('payload', payload);
      this.messageSource.next(payload);
    });
  }


}
