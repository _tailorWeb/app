export interface UserDto {
  /* Id of the user in firestore */
  id?: string
  /* uid of the user */
  uid: string
  /* Email of the user */
  email: string
  /* photoUrl of the user */
  photoURL?: string
  /* Display name of the user */
  displayName?: string
  /* If the account is synced with the DataDash App */
  isSynced?: boolean
}

