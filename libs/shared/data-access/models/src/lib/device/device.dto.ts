export interface DeviceFrame {
  rxPacketsReceived: number,
  rxPacketsReceivedOK: number,
  timestamp: string,
  txPacketsEmitted: number,
  txPacketsReceived: number,
}

interface DeviceResultLocation {
  'latitude': number,
  'longitude': number,
  'altitude': number,
  'source': 'string',
  'accuracy': number
}

interface DeviceResultItem {
  'id': string,
  'name': string,
  'description': string,
  'createdAt': string,
  'updatedAt': string,
  'firstSeenAt': string,
  'lastSeenAt': string,
  'organizationID': string,
  'networkServerID': string,
  'location': DeviceResultLocation,
  'frames'?: DeviceFrame[]
}

export interface DeviceDto {
  /* Number of m2 Pro Miner */
  totalCount: number
  /* Total Frames received for all the m2 Pro Miner grouped */
  totalFrames?: number
  /* Result of the m2 Pro Miner */
  result: DeviceResultItem[]
}
