export interface SymbolStatsDto {
  max: number,
  min: number,
  mean: number,
  isBullish: boolean,
}
