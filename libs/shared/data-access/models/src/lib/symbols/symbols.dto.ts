import { SymbolStatsDto } from './symbol-stats.dto'

export interface SymbolsDto {
  /* Actual Price */
  buy: number
  /* Evolution of the price in the last 24h */
  rose: number
  /* Stats */
  stats: SymbolStatsDto
}
