interface ActStake {
  id: string;
  startTime: Date;
  endTime: null | Date;
  amount: string;
  active: boolean;
  lockTill: null | Date;
  boost: string;
  revenue: string;
}

export interface MXCStackingDto {
  /* MXC Stacking */
  actStake: ActStake[]
}

export interface MXCGlobalStackDto {
  /* Total of MXC Stacking */
  amount: string
  /* Total of MXC Stacking in Usd */
  amountUsd: string
  /* Total of MXC Revenue */
  revenue: string
  /* Total of MXC Revenue in Usd */
  revenueUsd: string

}
