export interface MXCWalletDto {
  /* Your MXC Wallet Balance */
  balance: string

    /* Your MXC Wallet Balance Usd */
  balanceUsd: string
}
