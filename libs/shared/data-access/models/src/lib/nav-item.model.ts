export interface NavItem {
  label: string
  path: string
  icon: string
  color?: string
  hideOnNavbar?: boolean
}
