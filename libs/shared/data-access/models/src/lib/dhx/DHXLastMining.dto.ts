export interface DHXLastMiningDto {
  /* Date */
  date: Date
  /* Mining Power of the supernode */
  miningPower: string
  /* DHX Allocated */
  dhxAllocated: string
  /* DHX Amount */
  dhxAmount: string
  /* Id of the organisation */
  orgId: string
  /* My Mining Power */
  orgMiningPower: string
  /* my DHX Limit */
  orgDhxLimit: string
  /* Amount of DHX earned */
  orgDhxAmount: string
  /* Amount of DHX earned */
  orgDhxAmountValue: string
  /* Percentage of optimal */
  optimality: string
  /* Earning at the current price */
  earning: string
}
