export interface DhxMiningInterface {
  miningDate: string
  orgId: string,
  orgMiningPower: string,
  orgDhxBonded: string,
  orgDhxMined: string
}

export interface DhxMiningGraphInterface {
  name: string;
  value: string;
}

export interface DHXHistoryDto {
  /* DHX Mining */
  dhxMining: [{ name: string, series: DhxMiningGraphInterface[] }]
  /* Global DHX Mining Earning */
  globalEarning: string
  /* Global DHX Mining Earning in USD */
  globalEarningUsd: string
}
