interface Council {
  id: string;
  chairOrgId: string;
  name: string;
  lastPaidDate: string;
  lastMpower: string;
  lastDhxRevenue: string;
}

export interface DHXCouncilDto {
  /* List of council */
  council: Council[]
}
