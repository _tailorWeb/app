interface BondedItem {
  amount: string,
  created: Date
}

export interface DHXBondedDto {
  /* DHX Bonded */
  dhxBonded: string
  /* DHX Bonded Value in $ */
  dhxBondedValue: string
  /* Evolution of the price in $ in the last 24h */
  dhxCoolingOffTotal: number
  /* DHX Cooling Off */
  dhxCoolingOff: BondedItem[]
  /* DHX Ubonding */
  dhxUnbonding: BondedItem[]
  /* Total of DHX unbonded */
  dhxUnbondingTotal: string
  /* OrgaId */
  orgId: string
}
