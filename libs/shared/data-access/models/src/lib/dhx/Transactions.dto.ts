import { DhxMiningGraphInterface } from './DHXHistory.dto'

export interface TxHistoryDetail {
  ID: string,
  WalletID: string,
  Amount: string,
  TopUpFee: string,
  SenderAddress: string
  Boost?: string
  LockTill?: string
  StartTime?: string
  TxApprovedTime: string
  TxHash: string
  CustodyService: string
  CustodyTopUpID: string
}

export interface TxHistory {
  id: string,
  title?: string,
  timestamp: string,
  amount: string,
  paymentType: string,
  minings?: [name: string, value: string]
  detailsJson: string | TxHistoryDetail
}

export interface TransactionsDto {
  /* Transaction Item */
  tx: TxHistory[]
  /* Mining MXC Item */
  minings?: [{ name: string, series: DhxMiningGraphInterface[] }]
  /* Total Mining MXC */
  miningsTotal?: string
  /* Total Mining MXC USD */
  miningsTotalUSD?: string
}
