export interface ProfileDto {
  /* Username of the user */
  username: string

  /* Email of the user */
  email: string
}
