import { DOCUMENT } from '@angular/common'
import { Inject, Injectable } from '@angular/core'
import { ImmerComponentStore } from 'ngrx-immer/component-store'
import { catchError, switchMap, take, tap } from 'rxjs/operators'
import {
  NavItem,
  UserDto,
} from '@simulator/shared/data-access/models'
import { AuthService } from '@simulator/shared/data-access/services'
import { SyncService } from '@simulator/shared/ui/sync'
import { UiIcon } from '@simulator/shared/ui/icon'
import { forkJoin, of } from 'rxjs'

type UiTheme = 'dark' | 'light'

interface UiState {
  theme: UiTheme,
  user: null | UserDto,
  error: null | string
  branding: null | { logoPath: string }
  showMenu: boolean,
  navItems: NavItem[]
}

const LS_THEME_KEY = '@@MXCSimulator/theme'

@Injectable({ providedIn: 'root' })
export class UiStore extends ImmerComponentStore<UiState> {
  private readonly body: HTMLElement

  constructor(@Inject(DOCUMENT) private document: Document,
              private readonly authService: AuthService,
              private readonly syncService: SyncService,
  ) {
    super({
      theme: 'light',
      user: null,
      error: null,
      showMenu: false,
      branding: null,
      navItems: [
        {
          label: 'Dashboard',
          path: '/home',
          icon: 'fire',
        },
        {
          label: 'Stats',
          path: '/stats',
          icon: 'sparkles',
        },
        {
          label: 'Forecast',
          path: '/forecast',
          icon: 'pie',
        },
        {
          label: 'Suggestion',
          path: '/suggestion',
          icon: 'chat',
        },
        {
          label: 'Faq',
          path: '/faq',
          icon: 'faq',
        },
      ],
    })
    this.body = this.document.body
    this.initializeEffect()
    this.toggleMenuEffect(this.select((state) => state.showMenu))
    this.toggleThemeEffect(this.select((state) => state.theme))
  }

  readonly navItems$ = this.select((s) => s.navItems)

  readonly vm$ = this.select(({
                                theme, showMenu,
                                error, branding,
                                navItems, user,
                              }) => ({
    theme,
    showMenu,
    error,
    branding,
    navItems,
    user,
    icon: theme === 'dark' ? UiIcon.sun : UiIcon.moon,
  }))

  readonly initializeEffect = this.effect(($) =>
    $.pipe(
      tap(() => {
        const savedTheme = localStorage.getItem(LS_THEME_KEY)
        if (savedTheme) {
          this.patchState({ theme: savedTheme as UiTheme })
        }
      }),
      switchMap(_ => {
        return forkJoin([
          this.authService.getUser().pipe(
            tap(user => {
              if (user && !user.isSynced) {
                /* Show the modal to sync the user */
                this.syncService.show(false)
              }
              this.patchState({ user })
            })),
        ])
      }),
      catchError(err => {
        console.log('err', err)
        const error = err.message
        this.patchState({ error })
        return of(error)
      }),
    ),
  )

  readonly toggleTheme = this.updater((state) => {
    state.theme = state.theme === 'dark' ? 'light' : 'dark'
  })

  readonly toggleMenu = this.updater((state) => {
    state.showMenu = !state.showMenu
  })

  readonly openMenu = this.updater((state) => {
    state.showMenu = true
  })

  readonly closeMenu = this.updater((state) => {
    state.showMenu = false
  })

  private readonly toggleMenuEffect = this.effect<boolean>((showMenu$) =>
    showMenu$.pipe(
      tap((showMenu: boolean) => {
        if (showMenu) {
          this.body.classList.add('overflow-hidden')
        } else {
          this.body.classList.remove('overflow-hidden')
        }
      }),
    ),
  )

  private readonly toggleThemeEffect = this.effect<UiTheme>((theme$) =>
    theme$.pipe(
      tap((theme: UiTheme) => {
        localStorage.setItem(LS_THEME_KEY, theme)
        this.body.classList.remove('dark', 'light')
        this.body.classList.add(theme)
      }),
    ),
  )
}
