import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TilesComponent } from './tiles.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { SharedItemLoadModule } from '@simulator/shared/item-load'
import { WebHomeUiRoseIndicatorModule } from '@simulator/web/home/ui/rose-indicator'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule,
    SharedItemLoadModule,
    WebHomeUiRoseIndicatorModule,
  ],
  declarations: [
    TilesComponent,
  ],
  exports: [TilesComponent],
})
export class SharedUiTilesModule {
}
