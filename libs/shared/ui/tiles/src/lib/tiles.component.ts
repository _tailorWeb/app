import { Component, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-tiles',
  templateUrl: './tiles.component.html',
  styles: [
    `
        :host {
            display: block;
        }
        .tile-container {
            min-height: 89px;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TilesComponent {
  @Input() title: string;
  @Input() value;
  @Input() logo: string;
  @Input() link: string = '#';
  @Input() icon: string;
  @Input() currency: string;

}
