import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { TilesComponent } from './tiles.component'


describe('AlertComponent', () => {
  let spectator: Spectator<TilesComponent>
  const createComponent = createComponentFactory(TilesComponent)

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
