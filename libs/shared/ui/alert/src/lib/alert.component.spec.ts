import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { AlertComponent } from './alert.component'


describe('AlertComponent', () => {
  let spectator: Spectator<AlertComponent>
  const createComponent = createComponentFactory(AlertComponent)

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
