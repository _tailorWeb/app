import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule
  ],
  declarations: [
    AlertComponent
  ],
  exports: [
    AlertComponent
  ],
})
export class SharedUiAlertModule {}
