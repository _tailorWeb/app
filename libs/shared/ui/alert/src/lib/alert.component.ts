import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'simulator-alert',
  templateUrl: './alert.component.html',
  styles: [
    `
        :host {
            display: block;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() canClose: boolean = false;
  @Input() logo: string = 'question';
  @Input() colorLogo: string = 'text-green-400';
  @Input() colorText: string = 'dark:text-white text-gray-900';
  @Input() colorSubtitle: string = 'text-gray-500 dark:text-white';

  constructor() {
  }

  ngOnInit(): void {
  }

}
