import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UiStore } from '@simulator/shared/data-access/ui-store'
import { UiIcon } from '@simulator/shared/ui/icon'
import { SyncService } from '@simulator/shared/ui/sync'

@Component({
  selector: 'simulator-header',
  templateUrl: './header.component.html',
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  vm$ = this.service.vm$
  uiIcon = UiIcon;
  toggleDrawer = false;

  constructor(
    private readonly service: UiStore,
    private readonly syncService: SyncService,
  ) {}

  toggleDarkMode(): void {
    this.service.toggleTheme();
  }

  syncHandler(isSynced: boolean) {
    this.syncService.show(isSynced);
  }
}
