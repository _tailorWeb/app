import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HeaderComponent } from './header.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { RouterModule } from '@angular/router'
import { SharedItemLoadModule } from '@simulator/shared/item-load'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedUiIconModule,
    SharedItemLoadModule,
  ],
  declarations: [
    HeaderComponent,
  ],
  exports: [
    HeaderComponent,
  ],
})
export class SharedUiHeaderModule {
}
