import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ItemLoadComponent } from './item-load.component'

@NgModule({
  imports: [CommonModule],
  declarations: [
    ItemLoadComponent,
  ],
  exports: [ItemLoadComponent],
})
export class SharedItemLoadModule {
}
