import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { ItemLoadComponent } from './item-load.component'


describe('LoadingComponent', () => {
  let spectator: Spectator<ItemLoadComponent>

  const createComponent = createComponentFactory(ItemLoadComponent)

  beforeEach(() => {
    spectator = createComponent()
  })

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })

  it('should NOT show div', () => {
    expect(spectator.query('div')).not.toBeTruthy()
  })

  it('should show div if loading true', () => {
    spectator.setInput('loading', true)
    expect(spectator.query('div')).toBeTruthy()
  })
})
