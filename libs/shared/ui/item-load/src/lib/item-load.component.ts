import { Component, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-item-load',
  templateUrl: './item-load.component.html',
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemLoadComponent {
  @Input() loading = true
}
