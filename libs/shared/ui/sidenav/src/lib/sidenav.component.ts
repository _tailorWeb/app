import {
  Component, ChangeDetectionStrategy,
} from '@angular/core'
import { UiStore } from '@simulator/shared/data-access/ui-store'
import { UiIcon } from '@simulator/shared/ui/icon'
import { AuthService } from '@simulator/shared/data-access/services'
import { SyncService } from '@simulator/shared/ui/sync'
import { Router } from '@angular/router'
import { HammerGestureConfig } from '@angular/platform-browser'
import { fromEvent } from 'rxjs'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { DeviceDetectorService } from 'ngx-device-detector'

@UntilDestroy()
@Component({
  selector: 'simulator-sidenav',
  templateUrl: './sidenav.component.html',
  styles: [
    `
        :host {
            display: block;
        }

        .show-item {
            color: #fff !important;
        }

        .is-active-menu {
            color: #fff;
            font-weight: bold;
        }

        .margin-sidenav {
            margin-top: 3.3em;
        }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidenavComponent {
  vm$ = this.service.vm$
  uiIcon = UiIcon

  constructor(private readonly service: UiStore,
              private readonly router: Router,
              private readonly syncService: SyncService,
              private readonly deviceDetector: DeviceDetectorService,
              private readonly authService: AuthService) {
    this.handleSwipe()
  }

  handleSwipe(){
    const isMobile = this.deviceDetector.isMobile();
    const hammerConfig = new HammerGestureConfig()
    const hammer = hammerConfig.buildHammer(document.documentElement)
    fromEvent(hammer, 'swipe').pipe(untilDestroyed(this))
      .subscribe((res: any) => {
        if (isMobile && res.deltaX > 0) {
          // this.openMenu()
        } else if (isMobile) {
          this.closeMenu()
        }
      })
  }

  toggleMenu(): void {
    this.service.toggleMenu()
  }

  toggleDarkMode(): void {
    this.service.toggleTheme()
  }

  openMenu() {
    this.service.openMenu()
  }

  closeMenu() {
    this.service.closeMenu()
  }


  syncHandler(isSynced: boolean) {
    this.syncService.show(isSynced);
  }

  async logout(){
    await this.authService.logout()
    return this.router.navigate(['/auth/sign-in']);
  }
}
