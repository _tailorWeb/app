import { Injectable, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SidenavComponent } from './sidenav.component'
import { RouterModule } from '@angular/router'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule } from '@angular/platform-browser'
import * as Hammer from 'hammerjs'

@Injectable()
export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { direction: Hammer.DIRECTION_ALL },
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedUiIconModule,
    HammerModule,
  ],
  declarations: [
    SidenavComponent,
  ],
  providers: [{ provide: HAMMER_GESTURE_CONFIG, useClass: HammerConfig }],
  exports: [SidenavComponent],
})
export class SharedUiSidenavModule {
}
