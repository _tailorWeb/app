import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { HelperComponent } from './helper.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    HelperComponent
  ],
  exports: [
    HelperComponent
  ],
})
export class SharedUiHelperModule {}
