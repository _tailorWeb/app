import { Injectable } from '@angular/core';
import { DialogService } from '@ngneat/dialog'
import { HelperComponent } from '../helper.component'

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  dialogRef;

  constructor(private readonly dialog: DialogService) {
  }

  start(params: {title: string, message: string}) {
    this.dialogRef = this.dialog.open(HelperComponent, {
      closeButton: false,
      enableClose: true,
      size: 'md',
      data: {
        title: params.title,
        message: params.message
      }
    });
  }

  close() {
    this.dialogRef.close();
  }


}
