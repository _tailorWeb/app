import { Component, ChangeDetectionStrategy } from '@angular/core';
import { DialogRef } from '@ngneat/dialog'

@Component({
  selector: 'simulator-helper',
  templateUrl: './helper.component.html',
  styles: [
    `
        ::ng-deep .ngneat-dialog-content {
            background: transparent !important;
            box-shadow: none !important;
            animation: none !important;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelperComponent {

  title: string;
  message: string;

  constructor(public ref: DialogRef) {}

}
