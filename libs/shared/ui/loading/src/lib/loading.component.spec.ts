import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { LoadingComponent } from './loading.component'


describe('LoadingComponent', () => {
  let spectator: Spectator<LoadingComponent>;

  const createComponent = createComponentFactory(LoadingComponent);

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('ref should be accessible', () => {
    expect(spectator.component.ref).toBeDefined();
  });

});
