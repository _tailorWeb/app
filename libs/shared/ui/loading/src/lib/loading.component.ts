import { Component, ChangeDetectionStrategy } from '@angular/core';
import { DialogRef } from '@ngneat/dialog';

@Component({
  selector: 'simulator-loading',
  templateUrl: './loading.component.html',
  styles: [
    `
        ::ng-deep .ngneat-dialog-content {
            background: transparent !important;
            box-shadow: none !important;
            animation: none !important;
        }
        svg {
            -webkit-transform-origin: (0.5px * 40) (0.5px * 40) 0;
            -moz-transform-origin: (0.5px * 40) (0.5px * 40) 0;
            -ms-transform-origin: (0.5px * 40) (0.5px * 40) 0;
            -o-transform-origin: (0.5px * 40) (0.5px * 40) 0;
            transform-origin: (0.5px * 40) (0.5px * 40) 0;
            -webkit-animation: spinner 4s linear infinite;
            -moz-animation: spinner 4s linear infinite;
            -ms-animation: spinner 4s linear infinite;
            -o-animation: spinner 4s linear infinite;
            animation: spinner 4s linear infinite;
        }
        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                stroke-dashoffset: (0.66 * 40);
            } 50% {
                  -webkit-transform: rotate(720deg);
                  stroke-dashoffset: (3.14 * 40);
              } 100% {
                    -webkit-transform: rotate(1080deg);
                    stroke-dashoffset: (0.66 * 40);
                }
        }

        @-moz-keyframes spinner {
            0% {
                -moz-transform: rotate(0deg);
                stroke-dashoffset: (0.66 * 40);
            } 50% {
                  -moz-transform: rotate(720deg);
                  stroke-dashoffset: (3.14 * 40);
              } 100% {
                    -moz-transform: rotate(1080deg);
                    stroke-dashoffset: (0.66 * 40);
                }
        }

        @-ms-keyframes spinner {
            0% {
                -ms-transform: rotate(0deg);
                stroke-dashoffset: (0.66 * 40);
            } 50% {
                  -ms-transform: rotate(720deg);
                  stroke-dashoffset: (3.14 * 40);
              } 100% {
                    -ms-transform: rotate(1080deg);
                    stroke-dashoffset: (0.66 * 40);
                }
        }

        @-o-keyframes spinner {
            0% {
                -o-transform: rotate(0deg);
                stroke-dashoffset: (0.66 * $spinnerSize);
            } 50% {
                  -o-transform: rotate(720deg);
                  stroke-dashoffset: (3.14 * $spinnerSize);
              } 100% {
                    -o-transform: rotate(1080deg);
                    stroke-dashoffset: (0.66 * $spinnerSize);
                }
        }

        @keyframes spinner {
            0% {
                transform: rotate(0deg);
                stroke-dashoffset: (0.66 * $spinnerSize);
            } 50% {
                  transform: rotate(720deg);
                  stroke-dashoffset: (3.14 * $spinnerSize);
              } 100% {
                    transform: rotate(1080deg);
                    stroke-dashoffset: (0.66 * $spinnerSize);
                }
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingComponent {

  constructor(public ref: DialogRef) {}

}
