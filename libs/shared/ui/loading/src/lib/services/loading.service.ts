import { Injectable } from '@angular/core'
import { DialogService } from '@ngneat/dialog'
import { LoadingComponent } from '../loading.component'

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  dialogRef

  constructor(private readonly dialog: DialogService) {
  }

  start(params: { title: string, message: string }) {
    this.dialogRef = this.dialog.open(LoadingComponent, {
      closeButton: false,
      enableClose: false,
      size: 'md',
      data: {
        title: params.title,
        message: params.message,
      },
    })
  }

  close() {
    if (this.dialogRef) {
      this.dialogRef.close()
      this.dialogRef  = null;
    }
  }


}
