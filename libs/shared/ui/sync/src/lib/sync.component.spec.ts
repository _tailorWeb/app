import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { SyncComponent } from './sync.component'


describe('SyncComponent', () => {
  let spectator: Spectator<SyncComponent>
  const createComponent = createComponentFactory(SyncComponent)

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
