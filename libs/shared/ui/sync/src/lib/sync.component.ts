import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { DialogRef, DialogService } from '@ngneat/dialog'
import { Supernode } from './supernode'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoadingService } from '@simulator/shared/ui/loading'
import { AuthService } from '@simulator/shared/data-access/services'
import { Observable, Subject } from 'rxjs'

@Component({
  selector: 'simulator-sync',
  templateUrl: './sync.component.html',
  styles: [
    `
        ::ng-deep .ngneat-dialog-content {
            background: transparent !important;
            box-shadow: none !important;
            animation: none !important;
        }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SyncComponent implements OnInit {
  supernodes = Supernode
  syncForm: FormGroup
  toggleMenu = false
  error = new Subject<string>()
  error$: Observable<string> = this.error.asObservable()
  constructor(public readonly dialog: DialogService,
              public ref: DialogRef,
              private readonly authService: AuthService,
              private readonly loading: LoadingService,
              private readonly fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.syncForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      supernode: ['MatchX EU', [Validators.required]],
    })
  }

  get supernode() {
    return this.syncForm.get('supernode')
  }

  getLogoSupernode(supernode: string) {
    return this.supernodes[supernode].logo
  }

  updateSupernode(supernode: string) {
    this.supernode.setValue(supernode)
    this.toggleMenuHandler()
  }

  toggleMenuHandler() {
    this.toggleMenu = !this.toggleMenu
  }

  async syncHandler() {
    try {
      this.loading.start({ title: 'Synchronizing your account', message: 'Please wait...' })
      const api = this.supernodes[this.supernode.value].url
      await this.authService.sync({ ...this.syncForm.value, api })
      this.loading.close()
      this.dialog.closeAll()
      setTimeout(()=>{
          window.location.reload();
      },500)
    } catch (e) {
      this.loading.close()
      if (e.includes('401')) {
        this.error.next('Your credentials do not allow access to the DataDash App')
      } else {
        this.error.next(e)
      }
    }
  }

}
