import { Injectable } from '@angular/core'
import { DialogService } from '@ngneat/dialog'
import { SyncComponent } from '../sync.component'

@Injectable({
  providedIn: 'root',
})
export class SyncService {
  dialogRef

  constructor(private readonly dialog: DialogService) {
  }

  show(isSynced: boolean) {
    this.dialogRef = this.dialog.open(SyncComponent, {
      closeButton: false,
      enableClose: false,
      size: 'md',
      data: {
        isSynced,
      },
    })
  }

  close() {
    this.dialogRef.close()
  }


}
