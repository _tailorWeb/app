/* Fetch from https://github.com/mxc-foundation/supernode-list/blob/master/supernode.json */
export const Supernode =
  {
    'Enlink': {
      'region': 'Korea',
      'url': 'https://lora.rosanetworks.com',
      'logo': 'https://lora.rosanetworks.com/branding.png',
      'status': 'online',
    },
    'XY': {
      'region': 'China',
      'url': 'https://mxcxy.com',
      'logo': 'https://mxcxy.com/branding.png',
      'status': 'online',
    },
    'Huaweitech': {
      'region': 'China',
      'url': 'https://lora.hunanhuaweikeji.com',
      'logo': 'https://lora.hunanhuaweikeji.com/branding.png',
      'status': 'online',
    },
    'MatchX EU': {
      'region': 'Europe',
      'url': 'https://lora.supernode.matchx.io',
      'logo': 'https://lora.supernode.matchx.io/branding.png',
      'status': 'online',
    },
    'MatchX AU': {
      'region': 'Oceania',
      'url': 'https://ausn.matchx.io',
      'logo': 'https://ausn.matchx.io/branding.png',
      'status': 'maintenance',
    },
    'DU IOT': {
      'region': 'Asia',
      'url': 'https://supernode.iot-ducapital.net',
      'logo': 'https://supernode.iot-ducapital.net/branding.png',
      'status': 'online',
    },
    'MatchX US': {
      'region': 'America',
      'url': 'https://ussn.matchx.io',
      'logo': 'https://ussn.matchx.io/branding.png',
      'status': 'online',
    },
    'Sejong': {
      'region': 'Korea',
      'url': 'https://k-supernode.com',
      'logo': 'https://k-supernode.com/branding.png',
      'status': 'online',
    },
  }
