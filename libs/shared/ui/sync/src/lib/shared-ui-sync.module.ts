import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { SyncComponent } from './sync.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule,
    ReactiveFormsModule,
    SharedUiAlertModule
  ],
  declarations: [
    SyncComponent
  ],
  exports: [
    SyncComponent
  ],
})
export class SharedUiSyncModule {}
