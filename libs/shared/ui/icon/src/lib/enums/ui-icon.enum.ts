export enum UiIcon {
  burger = 'burger',
  cube = 'cube',
  question = 'question',
  moon = 'moon',
  spinner = 'spinner',
  sun = 'sun',
  close = 'close',
  up = 'up',
  down = 'down',
  dollar = 'dollar',
  scale = 'scale',
  lightning = 'lightning',
  calendar = 'calendar',
  fire = 'fire',
  sparkles = 'sparkles',
  check = 'check',
  shopping = 'shopping',
  warning = 'warning',
  light = 'light',
  pie = 'pie',
  facebook = 'facebook',
  google = 'google',
  twitter = 'twitter',
  dot = 'dot',
  logout = 'logout',
  selector = 'selector',
  switchVertical = 'switchVertical',
  chat = 'chat',
  faq = 'faq',
}
