import { Global, Module } from '@nestjs/common'
import * as admin from 'firebase-admin'
import { ConfigModule } from '@nestjs/config'
import { FirebaseAdminModule } from '@tfarras/nestjs-firebase-admin'
import { firebaseConfiguration, FirebaseConfig } from '@simulator/configuration'

@Global()
@Module({
  imports: [
    FirebaseAdminModule.forRootAsync({
      imports: [ConfigModule.forFeature(firebaseConfiguration)],
      useFactory: (firebaseConfig: FirebaseConfig) => ({
        credential: admin.credential.cert(JSON.parse(firebaseConfig.FIREBASE_CERT)),
      }),
      inject: [firebaseConfiguration.KEY],
    }),
  ],
})
export class FirebaseGlobalModule {
}
