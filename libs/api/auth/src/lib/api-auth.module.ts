import { Module } from '@nestjs/common'
import { FirebaseGlobalModule } from './modules/firebase-global.module'
import { FirebaseStrategy } from './strategies/firebase.strategy'

@Module({
  imports: [FirebaseGlobalModule],
  providers: [FirebaseStrategy],
  exports: [],
})
export class ApiAuthModule {}
