import { Inject, Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { FirebaseAuthStrategy, FirebaseUser } from '@tfarras/nestjs-firebase-auth'
import { ExtractJwt } from 'passport-jwt'
import { FIREBASE_ADMIN_INJECT, FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin'
import { UserScope } from '@simulator/common'

@Injectable()
export class FirebaseStrategy extends PassportStrategy(FirebaseAuthStrategy, 'firebase') {
  public constructor(@Inject(FIREBASE_ADMIN_INJECT) private readonly firebaseAdmin: FirebaseAdminSDK) {
    super({
      extractor: ExtractJwt.fromAuthHeaderAsBearerToken(),
    })
  }

  async validate(payload: FirebaseUser): Promise<UserScope> {
    return this.firebaseAdmin.auth().getUser(payload.uid).then((user)=>{
      return {
        uid: user.uid,
        email: user.email,
        mxc_api: user.customClaims ? user.customClaims.mxc_api : null,
        mxc_username: user.customClaims ? user.customClaims.mxc_username : null,
        mxc_password: user.customClaims ? user.customClaims.mxc_password : null,
        mxc_user_id: user.customClaims ? user.customClaims.mxc_user_id : null,
        mxc_orga_id: user.customClaims ? user.customClaims.mxc_orga_id : null,
        mxc_jwt_token: user.customClaims ? user.customClaims.mxc_jwt_token : null,
      }
    })
  }
}
