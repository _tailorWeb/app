import { BadRequestException, Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import { SymbolsService } from './symbols.service'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { ApiErrors } from '@simulator/common'
import { SymbolsDto, SymbolStatsDto } from '@simulator/shared/data-access/models'
import { AuthGuard } from '@nestjs/passport'

@ApiTags('Symbols')
@UseGuards(AuthGuard('firebase'))
@ApiErrors()
@Controller()
export class SymbolsController {

  constructor(private readonly service: SymbolsService) {
  }

  @Get('mxc')
  @ApiOperation({ description: 'Getting MXC Price' })
  @ApiOkResponse()
  getMXC(): Promise<SymbolsDto> {
    return this.service.getMXCPrice()
  }

  @Get('dhx')
  @ApiOperation({ description: 'Getting DHX Price' })
  @ApiOkResponse()
  async getDHX(): Promise<SymbolsDto> {
    return this.service.getDHXPrice()
  }

  @Get('btc')
  @ApiOperation({ description: 'Getting BTC Price' })
  @ApiOkResponse()
  getBTC(): Promise<SymbolsDto> {
    return this.service.getBTCPrice()
  }

}
