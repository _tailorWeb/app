import { HttpService, Injectable } from '@nestjs/common'
import { InjectAppConfig } from '@simulator/config'
import type { AppConfig } from '@simulator/configuration'
import { map } from 'rxjs/operators'
import { SymbolsDto, SymbolStatsDto } from '@simulator/shared/data-access/models'
import * as analyse from 'technicalindicators'
import * as moment from 'moment'

@Injectable()
export class SymbolsService {
  baseUrl = `https://openapi.biki.cc/open/api`

  constructor(private readonly httpService: HttpService,
              @InjectAppConfig() readonly appConfig: AppConfig) {
  }

  async getMXCPrice(): Promise<SymbolsDto> {
    return this.httpService.get(`${this.baseUrl}/get_ticker?symbol=mxcusdt`).pipe(
      map(async ({ data }) => {
        return {
          buy: +parseFloat(data.data.buy).toFixed(4),
          rose: +(parseFloat(`${data.data.rose * 100}`).toFixed(2)),
          stats: await this.getStats('mxcusdt'),
        }
      })).toPromise()
  }

  async getDHXPrice(): Promise<SymbolsDto> {
    return this.httpService.get(`${this.baseUrl}/get_ticker?symbol=dhxusdt`).pipe(
      map(async ({ data }) => {
        return {
          buy: +parseFloat(data.data.buy).toFixed(2),
          rose: +(parseFloat(`${data.data.rose * 100}`).toFixed(2)),
          stats: await this.getStats('dhxusdt'),
        }
      })).toPromise()
  }

  async getBTCPrice(): Promise<SymbolsDto> {
    return this.httpService.get(`${this.baseUrl}/get_ticker?symbol=btcusdt`).pipe(
      map(async ({ data }) => {
        return {
          buy: +parseFloat(data.data.buy).toFixed(2),
          rose: +(parseFloat(`${data.data.rose * 100}`).toFixed(2)),
          stats: await this.getStats('btcusdt'),
        }
      })).toPromise()
  }

  getStats(symbol: 'btcusdt' | 'dhxusdt' | 'mxcusdt'): Promise<SymbolStatsDto> {
    /*K Line Cycle in Minutes, 1 for 1 minute, 1 Day for 1440 minutes 1/5/15/30/60/1440/10080/43200*/
    const period = 30
    /* https://github.com/code-biki/open-api/blob/master/README.us-en.md#Get-Klines */
    return this.httpService.get(`${this.baseUrl}/get_records?symbol=${symbol}&period=${period}`).pipe(
      map(({ data }) => {
        return this.analyseKLine(data.data, period)
      })).toPromise()
  }

  private analyseKLine(data, period) {
    const dates = data.map(el => el[0])
    const open = data.map(el => el[1])
    const high = data.map(el => el[2])
    const low = data.map(el => el[3])
    const close = data.map(el => el[4])
    const volume = data.map(el => el[5])

     /* https://github.com/anandanand84/technicalindicators/blob/master/lib/candlestick/Bullish.js */
    const isBullish = analyse.bullish({
      open,
      high,
      close,
      low,
    })
    // dates.map((date, i) => {
    //   console.log('date',
    //     moment(new Date(date * 1000)).format('MMMM Do YYYY, h:mm:ss'),
    //     '::',
    //     close[i],
    //   )
    // })
    const min = +parseFloat(Math.min(...close).toString()).toFixed(2)
    const max = +parseFloat(Math.max(...close).toString()).toFixed(2)
    const mean = +parseFloat(this.getMean(close).toString()).toFixed(2)
    return {
      min,
      max,
      mean,
      isBullish,
    }
  }

  private getMean(array: Array<number>) {
    return array.reduce((a, b) => a + b) / array.length
  }


}
