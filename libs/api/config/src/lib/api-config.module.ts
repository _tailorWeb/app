import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { appConfiguration, firebaseConfiguration, sendgridConfiguration } from '@simulator/configuration'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true,
      load: [appConfiguration, firebaseConfiguration, sendgridConfiguration],
    }),
  ],
})
export class ApiConfigModule {}
