import { Inject } from '@nestjs/common'
import { appConfiguration, firebaseConfiguration, sendgridConfiguration } from '@simulator/configuration'

export const InjectAppConfig = () => Inject(appConfiguration.KEY)
export const InjectFirebaseConfig = () => Inject(firebaseConfiguration.KEY)
export const InjectSendgridConfig = () => Inject(sendgridConfiguration.KEY)
