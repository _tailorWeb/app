import { Test, TestingModule } from '@nestjs/testing';
import { ApiProfileController } from './api-profile.controller';

describe('ApiProfileController', () => {
  let controller: ApiProfileController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiProfileController],
    }).compile();

    controller = module.get<ApiProfileController>(ApiProfileController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
