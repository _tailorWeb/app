import { Test, TestingModule } from '@nestjs/testing';
import { ApiProfileService } from './api-profile.service';

describe('ApiProfileService', () => {
  let service: ApiProfileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiProfileService],
    }).compile();

    service = module.get<ApiProfileService>(ApiProfileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
