import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Inject,
  Post,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { ApiProfileService } from './api-profile.service'
import { ProfileDto } from '@simulator/shared/data-access/models'
import { ApiErrors } from '@simulator/common'
import { AuthGuard } from '@nestjs/passport'
import { UserScope, UserReq } from '@simulator/common'
import { FIREBASE_ADMIN_INJECT, FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin'

@ApiTags('Profile')
@UseGuards(AuthGuard('firebase'))
@ApiErrors()
@Controller()
export class ApiProfileController {

  constructor(private readonly service: ApiProfileService,
              @Inject(FIREBASE_ADMIN_INJECT) private readonly firebaseAdmin: FirebaseAdminSDK) {
  }

  @Get()
  @ApiOperation({ description: 'Getting DataDash Profile' })
  @ApiOkResponse()
  async getProfile(@UserReq() user: UserScope): Promise<ProfileDto> {
    try {
      return await this.service.getProfile(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/branding')
  @ApiOperation({ description: 'Getting Branding logo' })
  @ApiOkResponse()
  async getBranding(@UserReq() user: UserScope): Promise<{ logoPath: string }> {
    try {
      return await this.service.getBranding(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Post('/synchronize')
  @ApiOperation({ description: 'Getting Branding logo' })
  @ApiOkResponse()
  async synchronize(@UserReq() user: UserScope, @Body() { username, password, api }): Promise<any> {
    try {
      return await this.service.processAuth({
        uid: user.uid,
        mxc_username: username,
        mxc_password: password,
        mxc_api: api,
      }).then(async ()=>{
        return await this.firebaseAdmin.firestore().doc(`users/${user.uid}`).update({isSynced: true});
      });
    } catch (e) {
      console.log("e", e.response.data);
      if (e.response.data.code === 13 || e.response.data.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }


  @Post('/suggestion')
  @ApiOperation({ description: 'Sending a suggestion' })
  @ApiOkResponse()
  suggestion(@UserReq() user: UserScope, @Body() { message }): Promise<any> {
      return this.service.sendSuggestion(message, user);
  }

}
