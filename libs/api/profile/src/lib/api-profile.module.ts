import { Module } from '@nestjs/common'
import { ApiProfileController } from './api-profile.controller';
import { ApiProfileService } from './api-profile.service';
import { CommonAPIModule } from '@simulator/common'

@Module({
  imports: [CommonAPIModule],
  controllers: [ApiProfileController],
  providers: [ApiProfileService],
  exports: [ApiProfileService],
})
export class ApiProfileModule {}
