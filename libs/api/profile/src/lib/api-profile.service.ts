import { Injectable } from '@nestjs/common'
import { ProfileDto } from '@simulator/shared/data-access/models'
import { MxcSdkService, UserScope } from '@simulator/common'

const sgMail = require('@sendgrid/mail')
import type { SendgridConfig } from '@simulator/configuration'
import { InjectSendgridConfig } from '@simulator/config'

@Injectable()
export class ApiProfileService {
  constructor(private readonly sdk: MxcSdkService,
              @InjectSendgridConfig() readonly sendgridConfig: SendgridConfig) {
  }

  processAuth(user: UserScope) {
    return this.sdk.processAuth(user)
  }

  /* Get Profile Endpoint */
  async getProfile(user: UserScope): Promise<ProfileDto> {
    if (!user.mxc_api || !user.mxc_username || !user.mxc_password) return
    return this.sdk.getProfile(user)
  }

  /* Get Supernode logo */
  getBranding(user: UserScope): Promise<{ logoPath: string }> {
    if (!user.mxc_api || !user.mxc_username || !user.mxc_password) return
    return this.sdk.getBranding(user)
  }

  /* Sending a suggestion */
  sendSuggestion(message: string, user: UserScope) {
    sgMail.setApiKey(this.sendgridConfig.SENDGRID_API)
    const msg = {
      to: 'contact@tailorweb.io',
      from: user.email,
      subject: 'contact form message myMXC.io',
      html: `<h1>Suggestion Form from myMXC.io</h1>
       <p>
         <b>Message: </b>${message}<br>
      </p>`,
    }
    return sgMail.send(msg)
  }


}
