import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm'
import { ApiPropertyOptional } from '@nestjs/swagger'

export abstract class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string

  @CreateDateColumn({ nullable: true })
  createdAt?: Date

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date
}

export abstract class BaseDto {
  @ApiPropertyOptional()
  id?: string

  @ApiPropertyOptional({ type: Date, format: 'date-time' })
  createdAt?: Date

  @ApiPropertyOptional({ type: Date, format: 'date-time' })
  updatedAt?: Date
}
