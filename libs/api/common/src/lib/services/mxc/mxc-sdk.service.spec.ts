import { Test, TestingModule } from '@nestjs/testing';
import { MxcSdkService } from './mxc-sdk.service';

describe('MxcSdkService', () => {
  let service: MxcSdkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MxcSdkService],
    }).compile();

    service = module.get<MxcSdkService>(MxcSdkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
