import { HttpService, Inject, Injectable, Scope } from '@nestjs/common'
import {
  DHXBondedDto,
  DHXCouncilDto,
  DHXHistoryDto,
  DHXLastMiningDto, DhxMiningInterface,
  TransactionsDto,
  ProfileDto, DeviceDto, DeviceFrame,
} from '@simulator/shared/data-access/models'
import { map, switchMap, catchError } from 'rxjs/operators'
import { FIREBASE_ADMIN_INJECT, FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin'
import { of } from 'rxjs'
import * as moment from 'moment'

export class UserScope {
  uid: string
  email?: string
  mxc_api: string
  mxc_username: string
  mxc_password: string
  mxc_user_id?: string
  mxc_orga_id?: string
  mxc_jwt_token?: string
}

@Injectable({
  scope: Scope.DEFAULT,
})
export class MxcSdkService {
  constructor(private readonly http: HttpService,
              @Inject(FIREBASE_ADMIN_INJECT) private readonly firebaseAdmin: FirebaseAdminSDK) {
  }

  /* Updating Claims on Firebase Token */
  updateClaim(user: UserScope) {
    return this.firebaseAdmin.auth().setCustomUserClaims(user.uid, {
      mxc_username: user.mxc_username,
      mxc_password: user.mxc_password,
      mxc_api: user.mxc_api,
      mxc_jwt_token: user.mxc_jwt_token || null,
      mxc_user_id: user.mxc_user_id || null,
      mxc_orga_id: user.mxc_orga_id || null,
    })
  }

  /* Process auth */
  async processAuth(user: UserScope) {
    const url = `${user.mxc_api}/api/internal/login`
    return this.http.post(url, {
      username: user.mxc_username,
      password: user.mxc_password,
    }).toPromise().then((login) => {
      if (!user.mxc_jwt_token) {
        return this.updateClaim({ ...user, mxc_jwt_token: login.data.jwt })
      }
    })

  }

  /* Get Profile Endpoint */
  async getProfile(user: UserScope): Promise<ProfileDto> {
    const url = `${user.mxc_api}/api/internal/profile`
    const headers = await this.getHeader(user)
    return this.catchError(user, this.http.get(url, { headers }).toPromise()).then(async (data) => {
      if (!user.mxc_orga_id && !user.mxc_user_id) {
        await this.updateClaim({
          ...user,
          mxc_orga_id: data.organizations[0].organizationID,
          mxc_user_id: data.user.id,
        })
      }

      return {
        username: data.user.username,
        email: data.user.email,
      }
    })
  }

  async getBranding(user: UserScope): Promise<{ logoPath: string }> {
    const url = `${user.mxc_api}/api/internal/branding`
    const headers = await this.getHeader(user)
    return this.catchError(user, this.http.get(url, { headers }).toPromise())
  }

  /***********************************
   **********MXC**********************
   ***********************************/

  /* Get list of active stakes */
  async getMXCStaking(user: UserScope): Promise<any> {
    const url = `${user.mxc_api}/api/staking/`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(`${url}/${user.mxc_orga_id}/activestakes`, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=mxcusdt`).pipe(
              map(({ data }) => {
                return { mxc: data.data.buy, ...res }
              }),
            )
          },
        ),
        map(res => {
          if (res.data.actStake) {
            const stacking = res.data.actStake
            res.data.amount = stacking.map(res => +res.amount).reduce((a, b) => a + b, 0)
            res.data.amountUsd = parseFloat((res.data.amount * res.mxc).toString()).toFixed(0)
            res.data.revenue = stacking.map(res => +res.revenue).reduce((a, b) => a + b, 0)
            res.data.revenueUsd = parseFloat((res.data.revenue * res.mxc).toString()).toFixed(0)
            return res
          }
        }),
      )
      .toPromise())
  }

  /* Get current Balance */
  async getCurrentWallet(user: UserScope): Promise<any> {
    const url = `${user.mxc_api}/api/wallet/balance`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(`${url}?orgId=${user.mxc_orga_id}&userId=${user.mxc_user_id}`, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=mxcusdt`).pipe(
              map(({ data }) => {
                return { mxc: data.data.buy, ...res }
              }),
            )
          },
        ),
        map(res => {
          res.data.balanceUsd = +parseFloat((+res.data.balance * res.mxc).toString()).toFixed(1)
          return res
        }),
      )
      .toPromise())
  }

  /* Get DHX Transaction history */
  async MXCTransactions(user: UserScope, dateFrom?: string, dateTo?: string): Promise<TransactionsDto> {
    const url = `${user.mxc_api}/api/wallet/tx-history`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    let till = moment().format('YYYY-MM-DDTHH:mm:ss')
    let from = moment().subtract(2, 'years').format('YYYY-MM-DDTHH:mm:ss')
    if (dateFrom && dateTo) {
      till = dateTo
      from = dateFrom
    }
    return this.catchError(user, this.http.get(`${url}?orgId=${user.mxc_orga_id}&currency=ETH_MXC&from=${from}Z&till=${till}Z`, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=mxcusdt`).pipe(
              map(({ data }) => {
                return { mxc: data.data.buy, ...res }
              }),
            )
          },
        ),
        map(res => {
          /* Fetch Minings Records only */
          let minings = res.data.tx.filter(el => el.paymentType === 'MINING').map(el => {
            el.detailsJson = JSON.parse(el.detailsJson)
            el.name = el.detailsJson.Timestamp.split('T')[0]
            el.amount = +el.amount
            return el
          })
          /* Group By Date */
          let miningsTotal = 0
          minings = Array.from(minings.reduce(
            (m, { name, amount }) => m.set(name, (m.get(name) || 0) + amount), new Map,
          ), ([name, amount]) => ({ name, amount })).map((el: any) => {
            el.value = +parseFloat(el.amount).toFixed(2)
            miningsTotal += el.amount
            return { name: el.name, value: el.value }
          })
          res.data.tx = res.data.tx.filter(el => el.paymentType !== 'MINING')
          res.data.minings = [{ name: 'MXC', series: minings }]
          res.data.miningsTotal = miningsTotal ? +parseFloat(miningsTotal.toString()).toFixed(2) : 0
          res.data.miningsTotalUSD = miningsTotal ? +parseFloat((miningsTotal * res.mxc).toString()).toFixed(2) : 0
          return res
        }),
      )
      .toPromise())
  }


  /***********************************
   **********DHX**********************
   ***********************************/

  /* Get info about bonded, cooling off and un-bonding DHX*/
  async bondInfoDHX(user: UserScope): Promise<DHXBondedDto> {
    const url = `${user.mxc_api}/api/dhx-mining/bond-info`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http
      .post(url, { orgId: user.mxc_orga_id }, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=dhxusdt`).pipe(
              map(({ data }) => {
                return { dhx: data.data.buy, ...res }
              }),
            )
          },
        ),
        map(res => {
          res.data.dhxBondedValue = +parseFloat((+res.data.dhxBonded * res.dhx).toString()).toFixed(1)
          res.data.dhxBonded = res.data.dhxBonded ? +parseFloat(res.data.dhxBonded).toFixed(2) : 0
          return res
        }))
      .toPromise())
  }

  /* Return info about DHX mining during the specified period */
  async DHXHistory(user: UserScope, dateFrom?: string, dateTo?: string): Promise<DHXHistoryDto> {
    const url = `${user.mxc_api}/api/dhx-mining/history`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    let till = moment().format('YYYY-MM-DDTHH:mm:ss')
    let from = moment().subtract(2, 'years').format('YYYY-MM-DDTHH:mm:ss')
    if (dateFrom && dateTo) {
      till = dateTo
      from = dateFrom
    }
    return this.catchError(user, this.http.get(`${url}?orgId=${user.mxc_orga_id}&from=${from}Z&till=${till}Z`, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=dhxusdt`).pipe(
              map(({ data }) => {
                return { dhx: data.data.buy, ...res }
              }),
            )
          },
        ),
        map((res: any) => {
          let globalEarning: unknown = 0
          let globalEarningUsd: unknown = 0
          let dhxMining = res.data.dhxMining
          if (res.data.dhxMining.length > 0) {
            globalEarning = res.data.dhxMining.map(el => +el.orgDhxMined).reduce((a, b) => a + b)
            globalEarning = parseFloat((+globalEarning).toString()).toFixed(2)
            globalEarningUsd = parseFloat((+globalEarning * res.dhx).toString()).toFixed(2)
            dhxMining = (res.data.dhxMining as []).map((item: DhxMiningInterface) => {
              return {
                value: +item.orgDhxMined,
                name: item.miningDate.split('T')[0],
              }
            })
          }
          return { data: { dhxMining: [{ name: 'DHX', series: dhxMining }], globalEarning, globalEarningUsd } }
        }))
      .toPromise())
  }

  /* Return info about the last paid day of DHX mining */
  async DHXLastMining(user: UserScope): Promise<DHXLastMiningDto> {
    const url =
      `${user.mxc_api}/api/dhx-mining/last-mining`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http
      .get(`${url}?orgId=${user.mxc_orga_id}`, { headers })
      .pipe(
        switchMap(res => {
            return this.http.get(`https://openapi.biki.cc/open/api/get_ticker?symbol=dhxusdt`).pipe(
              map(({ data }) => {
                return { dhx: data.data.buy, ...res }
              }),
            )
          },
        ),
        catchError(err => {
          if (err.message.includes('503')) {
            return of({
              data: {
                orgMiningPower: 0,
                optimality: 0,
                orgDhxAmount: 0,
                orgDhxLimit: 0,
                earning: 0,
                orgDhxAmountValue: 0,
              },
            })
          }
          return of(err)
        }),
        map(res => {
          res.data.orgMiningPower = +parseFloat(res.data.orgMiningPower).toFixed(0)
          res.data.optimality = (100 * res.data.orgDhxAmount) / res.data.orgDhxLimit
          res.data.optimality = res.data.optimality ? parseFloat(res.data.optimality).toFixed(0) + '%' : 0
          res.data.orgDhxAmount = +parseFloat(res.data.orgDhxAmount).toFixed(3)
          res.data.orgDhxLimit = +parseFloat(res.data.orgDhxLimit).toFixed(3)
          res.data.earning = res.data.orgDhxAmount ? +parseFloat((res.dhx * res.data.orgDhxAmount).toString()).toFixed(0) : 0
          res.data.orgDhxAmountValue = +parseFloat(res.data.orgDhxAmountValue).toFixed(2)
          return res
        }),
      )
      .toPromise())
  }

  /* Get DHX Transaction history */
  async DHXTransactions(user: UserScope, dateFrom?: string, dateTo?: string): Promise<TransactionsDto> {
    const url = `${user.mxc_api}/api/wallet/tx-history`
    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    let till = moment().format('YYYY-MM-DDTHH:mm:ss')
    let from = moment().subtract(2, 'years').format('YYYY-MM-DDTHH:mm:ss')
    if (dateFrom && dateTo) {
      till = dateTo
      from = dateFrom
    }
    return this.catchError(user, this.http.get(`${url}?orgId=${user.mxc_orga_id}&currency=DHX&from=${from}Z&till=${till}Z`, { headers }).toPromise())
  }

  /* List all existing councils */
  async DHXListCouncils(user: UserScope): Promise<DHXCouncilDto> {
    const url =
      `${user.mxc_api}/api/dhx-mining/list-councils`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(
      `${url}`
      , { headers }).toPromise())
  }

  /* List all dhx stakes for the organizations */
  async DHXListStacks(user: UserScope): Promise<any> {
    const url =
      `${user.mxc_api}/api/dhx-mining/list-stakes`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(
      `${url}&organizationId=${user.mxc_orga_id}`
      , { headers }).toPromise())
  }

  /***********************************
   **********DEVICE*******************
   ************************************/

  /* List all devices in the organizations */
  async GetDeviceList(user: UserScope): Promise<DeviceDto> {
    const url =
      `${user.mxc_api}/api/gateways`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(
      `${url}?limit=100&organizationID=${user.mxc_orga_id}`, { headers }).toPromise())
  }

  /* Get Frames per Device */
  async GetFramesPerDevice(user: UserScope, deviceId: string, dateFrom?: string, dateTo?: string): Promise<DeviceFrame> {
    const url =
      `${user.mxc_api}/api/gateways/${deviceId}/stats`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    let till = moment().format('YYYY-MM-DDTHH:mm:ss')
    let from = moment().startOf('day').format('YYYY-MM-DDTHH:mm:ss')
    if (dateFrom && dateTo) {
      till = dateTo
      from = dateFrom
    }
    return this.catchError(user, this.http.get(
      `${url}?startTimestamp=${from}Z&endTimestamp=${till}Z&interval=day`, { headers }).toPromise())
  }

  /* Get Device configuration */
  async GetConfigDevice(user: UserScope, deviceId: string): Promise<any> {
    const url =
      `${user.mxc_api}/api/gateways/getconfig/${deviceId}`

    const headers = await this.getHeader(user)
    if (!user.mxc_orga_id) {
      /* If any ORGA_ID defined we need to call the profile endpoint */
      await this.getProfile(user)
    }
    return this.catchError(user, this.http.get(
      url, { headers }).toPromise())
  }

  /***********************************
   **********UTILS********************
   ************************************/
  async catchError(user, promise) {
    try {
      return await promise.then(res => res.data)
    } catch (e) {
      console.log('e.message', e.message)
      /* If JWT has expired */
      if (e.message.includes('401')) {
        try {
          user.mxc_jwt_token = null
          await this.processAuth(user)
          return await promise.then(res => res.data)
        } catch (e) {
          /* Is not synced anymore */
          await this.firebaseAdmin.firestore().doc(`users/${user.uid}`).update({ isSynced: false })
          return e.message
        }
      }
      return e.message
    }
  }

  /* Setting the header */
  async getHeader(user: UserScope) {
    if (!user.mxc_jwt_token) {
      await this.processAuth(user)
    }
    return {
      'Authorization': `Bearer ${user.mxc_jwt_token}`,
    }
  }
}
