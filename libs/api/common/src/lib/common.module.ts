import { HttpModule, Module } from '@nestjs/common'
import { MxcSdkService } from './services'

@Module({
  imports: [HttpModule],
  controllers: [],
  providers: [MxcSdkService],
  exports: [MxcSdkService],
})
export class CommonAPIModule {
}
