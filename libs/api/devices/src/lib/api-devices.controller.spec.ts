import { Test, TestingModule } from '@nestjs/testing';
import { ApiDevicesController } from './api-devices.controller';

describe('ApiDevicesController', () => {
  let controller: ApiDevicesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiDevicesController],
    }).compile();

    controller = module.get<ApiDevicesController>(ApiDevicesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
