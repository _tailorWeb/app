import { Module } from '@nestjs/common'
import { ApiDevicesController } from './api-devices.controller';
import { ApiDevicesService } from './api-devices.service';
import { CommonAPIModule } from '@simulator/common'

@Module({
  imports: [CommonAPIModule],
  controllers: [ApiDevicesController],
  providers: [ApiDevicesService],
  exports: [ApiDevicesService],
})
export class ApiDevicesModule {}
