import { Test, TestingModule } from '@nestjs/testing';
import { ApiDevicesService } from './api-devices.service';

describe('ApiDevicesService', () => {
  let service: ApiDevicesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiDevicesService],
    }).compile();

    service = module.get<ApiDevicesService>(ApiDevicesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
