import { BadRequestException, Controller, Get, Param, Query, UnauthorizedException, UseGuards } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { ApiErrors } from '@simulator/common'
import { ApiDevicesService } from './api-devices.service'
import { AuthGuard } from '@nestjs/passport'
import { UserScope, UserReq } from '@simulator/common'
import { DeviceDto, DeviceFrame } from '@simulator/shared/data-access/models'

@ApiTags()
@UseGuards(AuthGuard('firebase'))
@ApiErrors()
@Controller()
export class ApiDevicesController {
  constructor(private readonly service: ApiDevicesService) {
  }

  @Get('/')
  @ApiOperation({ description: 'Get the list of device connected' })
  @ApiOkResponse()
  async getDevices(@UserReq() user: UserScope): Promise<DeviceDto> {
    try {
      return await this.service.getDevices(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/frames/:deviceId')
  @ApiOperation({ description: 'Get the frames for a device' })
  @ApiOkResponse()
  async getDeviceFrames(@UserReq() user: UserScope,
                        @Param('deviceId') deviceId: string,
                        @Query('from') from: string,
                        @Query('to') to: string): Promise<DeviceFrame> {
    try {
      return await this.service.getFramesPerDevice(user, deviceId, from, to)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }
}
