import { Injectable } from '@nestjs/common'
import { MxcSdkService, UserScope } from '@simulator/common'
import {DeviceDto, DeviceFrame} from '@simulator/shared/data-access/models'
@Injectable()
export class ApiDevicesService {
  constructor(private readonly sdk: MxcSdkService) {
  }

  /* Get list of device */
  async getDevices(user: UserScope): Promise<DeviceDto> {
    if (!user.mxc_api || !user.mxc_username || !user.mxc_password) return
    return this.sdk.GetDeviceList(user)
  }

  /* Get device's frames */
  async getFramesPerDevice(user: UserScope, deviceId: string, from?: string, to?: string): Promise<DeviceFrame> {
    if (!user.mxc_api || !user.mxc_username || !user.mxc_password) return
    return this.sdk.GetFramesPerDevice(user, deviceId, from, to)
  }
}
