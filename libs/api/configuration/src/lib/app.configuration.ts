import { ConfigType, registerAs } from '@nestjs/config'

export const appConfiguration = registerAs('app', () => ({
  port: process.env.APP_PORT,
  host: process.env.APP_HOST,
  env: process.env.NODE_ENV,
}))

export type AppConfig = ConfigType<typeof appConfiguration>
