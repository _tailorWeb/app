import { ConfigType, registerAs } from '@nestjs/config'

export const sendgridConfiguration = registerAs('sendgrid', () => ({
  SENDGRID_API: process.env.SENDGRID_API,
}))

export type SendgridConfig = ConfigType<typeof sendgridConfiguration>
