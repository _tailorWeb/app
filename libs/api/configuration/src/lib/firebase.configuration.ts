import { ConfigType, registerAs } from '@nestjs/config'

export const firebaseConfiguration = registerAs('firebase', () => ({
  FIREBASE_CERT: process.env.FIREBASE_CERT,
}))

export type FirebaseConfig = ConfigType<typeof firebaseConfiguration>
