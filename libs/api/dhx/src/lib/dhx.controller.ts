import { BadRequestException, Controller, Get, Query, UnauthorizedException, UseGuards } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { DhxService } from './dhx.service'
import { DHXCouncilDto, DHXHistoryDto, DHXLastMiningDto,DHXBondedDto, TransactionsDto } from '@simulator/shared/data-access/models'
import { ApiErrors } from '@simulator/common'
import { AuthGuard } from '@nestjs/passport'
import { UserScope, UserReq } from '@simulator/common'

@ApiTags('DHX')
@UseGuards(AuthGuard('firebase'))
@ApiErrors()
@Controller()
export class DhxController {
  constructor(private readonly service: DhxService) {
  }

  @Get('/')
  @ApiOperation({ description: 'Getting Bonded DHX Info' })
  @ApiOkResponse()
  async getBondedInfo(@UserReq() user: UserScope): Promise<DHXBondedDto> {
    try {
      return await this.service.getBondedInfo(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/history')
  @ApiOperation({ description: 'Getting DHX History' })
  @ApiOkResponse()
  async getDHXHistory(@UserReq() user: UserScope,
                      @Query('from') from: string,
                      @Query('to') to: string): Promise<DHXHistoryDto> {
    try {
      return await this.service.getDHXHistory(user, from, to)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/last-mining')
  @ApiOperation({ description: 'Getting DHX Last Mining' })
  @ApiOkResponse()
  async getDHXLastMining(@UserReq() user: UserScope): Promise<DHXLastMiningDto> {
    try {
      return await this.service.getDHXLastMining(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/list-councils')
  @ApiOperation({ description: 'Getting DHX List councils' })
  @ApiOkResponse()
  async getDHXListCouncils(@UserReq() user: UserScope): Promise<DHXCouncilDto> {
    try {
      return await this.service.getDHXListCouncils(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/list-stacks')
  @ApiOperation({ description: 'Getting DHX List stacks' })
  @ApiOkResponse()
  async getDHXListStacks(@UserReq() user: UserScope): Promise<any> {
    try {
      return await this.service.getDHXListStacks(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }

  @Get('/transactions')
  @ApiOperation({ description: 'Getting DHX transactions' })
  @ApiOkResponse()
  async getDHXTransactions(@UserReq() user: UserScope,
                           @Query('from') from: string,
                           @Query('to') to: string): Promise<TransactionsDto> {
    try {
      return await this.service.getDHXTransactions(user, from, to)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }


}
