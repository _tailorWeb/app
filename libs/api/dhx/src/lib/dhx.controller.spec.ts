import { Test, TestingModule } from '@nestjs/testing';
import { DhxController } from './dhx.controller';

describe('DhxController', () => {
  let controller: DhxController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DhxController],
    }).compile();

    controller = module.get<DhxController>(DhxController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
