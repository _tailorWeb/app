import { Module } from '@nestjs/common'
import { DhxController } from './dhx.controller'
import { DhxService } from './dhx.service'
import { CommonAPIModule } from '@simulator/common'

@Module({
  imports: [CommonAPIModule],
  controllers: [DhxController],
  providers: [DhxService],
  exports: [DhxService],
})
export class ApiDhxModule {
}
