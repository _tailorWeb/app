import { Test, TestingModule } from '@nestjs/testing';
import { DhxService } from './dhx.service';

describe('DhxService', () => {
  let service: DhxService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DhxService],
    }).compile();

    service = module.get<DhxService>(DhxService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
