import { Injectable } from '@nestjs/common'
import { MxcSdkService, UserScope } from '@simulator/common'
import {
  DHXBondedDto,
  DHXCouncilDto,
  DHXHistoryDto,
  DHXLastMiningDto,
  TransactionsDto,
} from '@simulator/shared/data-access/models'

@Injectable()
export class DhxService {
  constructor(private readonly sdk: MxcSdkService) {
  }

  /* Get info about bonded, cooling off and un-bonding DHX*/
  async getBondedInfo(user: UserScope): Promise<DHXBondedDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.bondInfoDHX(user)
  }

  /* Return info about DHX mining during the specified period */
  async getDHXHistory(user: UserScope, from?: string, to?: string): Promise<DHXHistoryDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.DHXHistory(user, from, to)
  }

  /* Return info about DHX mining during the specified period */
  async getDHXLastMining(user: UserScope): Promise<DHXLastMiningDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.DHXLastMining(user)
  }

  /* List all existing councils */
  async getDHXListCouncils(user: UserScope): Promise<DHXCouncilDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.DHXListCouncils(user)
  }

  /* List all dhx stakes for the organizations */
  async getDHXListStacks(user: UserScope): Promise<any> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.DHXListStacks(user)
  }

  /* Return info about DHX mining during the specified period */
  async getDHXTransactions(user: UserScope, from?: string, to?: string): Promise<TransactionsDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.DHXTransactions(user, from, to)
  }

}
