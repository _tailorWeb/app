import { Test, TestingModule } from '@nestjs/testing';
import { ApiMxcController } from './api-mxc.controller';

describe('ApiMxcController', () => {
  let controller: ApiMxcController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiMxcController],
    }).compile();

    controller = module.get<ApiMxcController>(ApiMxcController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
