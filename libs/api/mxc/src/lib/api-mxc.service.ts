import { Injectable } from '@nestjs/common';
import { MxcSdkService, UserScope } from '@simulator/common'
import { MXCStackingDto, MXCWalletDto, TransactionsDto } from '@simulator/shared/data-access/models'

@Injectable()
export class ApiMxcService {
  constructor(private readonly sdk: MxcSdkService) {
  }

  getMXCStacking(user: UserScope): Promise<MXCStackingDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.getMXCStaking(user);
  }

  getCurrentWallet(user: UserScope): Promise<MXCWalletDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.getCurrentWallet(user);
  }

  getMXCTransactions(user: UserScope, from?: string, to?: string): Promise<TransactionsDto> {
    if (!user.mxc_api || !user.mxc_username|| !user.mxc_password) return;
    return this.sdk.MXCTransactions(user, from, to);
  }

}
