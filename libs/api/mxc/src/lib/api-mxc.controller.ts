import { BadRequestException, Controller, Get, Query, UnauthorizedException, UseGuards } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { MXCStackingDto, MXCWalletDto, TransactionsDto } from '@simulator/shared/data-access/models'
import { ApiMxcService } from './api-mxc.service'
import { ApiErrors } from '@simulator/common'
import { AuthGuard } from '@nestjs/passport'
import { UserScope, UserReq } from '@simulator/common'

@ApiTags('Mxc')
@UseGuards(AuthGuard('firebase'))
@ApiErrors()
@Controller()
export class ApiMxcController {
  constructor(private readonly service: ApiMxcService) {
  }

  @Get('/stacking')
  @ApiOperation({ description: 'Getting MXC Stacking' })
  @ApiOkResponse()
  async getMXCStacking(@UserReq() user: UserScope): Promise<MXCStackingDto> {
    try {
      return await this.service.getMXCStacking(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message);
      } else {
        throw new BadRequestException(e.response.data.message);
      }
    }
  }

  @Get('/wallet')
  @ApiOperation({ description: 'Getting MXC Current Wallet' })
  @ApiOkResponse()
  async getMXCWallet(@UserReq() user: UserScope): Promise<MXCWalletDto> {
    try {
      return await this.service.getCurrentWallet(user)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message);
      } else {
        throw new BadRequestException(e.response.data.message);
      }
    }
  }

  @Get('/transactions')
  @ApiOperation({ description: 'Getting MXC transactions' })
  @ApiOkResponse()
  async getDHXTransactions(@UserReq() user: UserScope,
                           @Query('from') from: string,
                           @Query('to') to: string): Promise<TransactionsDto> {
    try {
      return await this.service.getMXCTransactions(user, from, to)
    } catch (e) {
      if (e.response.code === 16) {
        throw new UnauthorizedException(e.response.data.message)
      } else {
        throw new BadRequestException(e.response.data.message)
      }
    }
  }


}
