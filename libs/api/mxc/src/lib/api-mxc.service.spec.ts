import { Test, TestingModule } from '@nestjs/testing';
import { ApiMxcService } from './api-mxc.service';

describe('ApiMxcService', () => {
  let service: ApiMxcService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiMxcService],
    }).compile();

    service = module.get<ApiMxcService>(ApiMxcService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
