import { Module } from '@nestjs/common'
import { ApiMxcController } from './api-mxc.controller';
import { ApiMxcService } from './api-mxc.service';
import { CommonAPIModule } from '@simulator/common'

@Module({
  imports: [CommonAPIModule],
  controllers: [ApiMxcController],
  providers: [ApiMxcService],
  exports: [ApiMxcService],
})
export class ApiMxcModule {}
