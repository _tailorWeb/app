import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';

import { SuggestionComponent } from './suggestion.component'

describe('SuggestionComponent', () => {
  let spectator: Spectator<SuggestionComponent>;

  const createComponent = createComponentFactory({
    component: SuggestionComponent,
    imports: [RouterTestingModule],
    providers: [
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
