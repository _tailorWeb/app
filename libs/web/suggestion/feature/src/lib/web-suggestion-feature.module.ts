import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { SuggestionComponent } from './suggestion.component'
import { RouterModule } from '@angular/router'
import { SharedUiPageModule } from '@simulator/shared/ui/page'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    SharedUiPageModule,
    ReactiveFormsModule,
    SharedUiAlertModule,
    RouterModule.forChild([
      {
        path: '',
        component: SuggestionComponent,
      },
    ]),
  ],
  declarations: [
    SuggestionComponent
  ],
})
export class WebSuggestionFeatureModule {}
