import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { SuggestionStore } from '@simulator/web/suggestion/data-access/suggestion-store'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { LoadingService } from '@simulator/shared/ui/loading'

@UntilDestroy()
@Component({
  selector: 'simulator-suggestion',
  templateUrl: './suggestion.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuggestionComponent implements OnInit {
  vm$ = this.service.vm$
  suggestionForm: FormGroup = this.fb.group({ message: ['', Validators.required] })

  constructor(private readonly fb: FormBuilder,
              private readonly loading: LoadingService,
              private readonly service: SuggestionStore) {
  }

  ngOnInit() {
    this.service.isLoading$.pipe(untilDestroyed(this)).subscribe((val) => {
      if (val) {
        this.loading.start({ title: 'Loading', message: 'Please wait while sending your suggestion...' })
      } else {
        this.suggestionForm.get('message').setValue('')
        this.loading.close()
      }
    })
  }

  get message() {
    return this.suggestionForm.get('message').value
  }

  send() {
    this.service.sendSuggestionEffect(this.message)
  }

}
