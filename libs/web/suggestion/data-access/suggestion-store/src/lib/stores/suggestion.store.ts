import { Injectable } from '@angular/core'
import { ImmerComponentStore } from 'ngrx-immer/component-store'
import { catchError, delay, switchMap, tap } from 'rxjs/operators'
import { AuthService } from '@simulator/shared/data-access/services'
import { Observable, of } from 'rxjs'
import { ToastrService } from 'ngx-toastr'


interface UiState {
  error: null | string,
  isLoading: boolean
}

@Injectable({ providedIn: 'root' })
export class SuggestionStore extends ImmerComponentStore<UiState> {

  constructor(private readonly authService: AuthService,
              private readonly toastr: ToastrService) {
    super({
      error: null,
      isLoading: false,
    })
  }

  readonly isLoading$ = this.select((s) => s.isLoading)

  readonly vm$ = this.select(({
                                error,
                              }) => ({
    error,
  }))


  readonly sendSuggestionEffect = this.effect((message$: Observable<string>) =>
    message$.pipe(
      tap(() => this.startLoading()),
      switchMap(message => this.authService.sendSuggestion(message)),
      tap(() => this.stopLoading()),
      tap(() => this.toastr.success(
        'Your message has been delivered successfully',
        'Thank you :)')),
      catchError(err => {
        const error = err.message
        this.patchState({ error, isLoading: false })
        return of(error)
      }),
    ))

  readonly startLoading = this.updater((state) => {
    state.isLoading = true
  })

  readonly stopLoading = this.updater((state) => {
    state.isLoading = false
  })


}
