import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core'
import { HomeStore } from '@simulator/web/home/data-access/home-store'
import * as moment from 'moment'
import { BehaviorSubject, of } from 'rxjs'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { LoadingService } from '@simulator/shared/ui/loading'
import { delay, tap } from 'rxjs/operators'

interface RewardItem {
  daily_mining_limit: string,
  expected_earning: string,
  optimal_percent: string,
}

export interface RewardResume {
  thirty_day_dhx?: string,
  thirty_day_usd?: string,
  optimal_percent?: number,
  optimal_percent_indicator?: number
  dhxBonded?: number,
  reinvestment?: string,
  reinvestment_mxc?: string,
  reinvestment_usd?: string,
  miningPower?: string,
  to_sell?: boolean,
  alert?: string,
}

@UntilDestroy()
@Component({
  selector: 'simulator-forecast',
  templateUrl: './forecast.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForecastComponent implements OnInit {
  vm$ = this.service.vm$
  DHXBonded: BehaviorSubject<{ bondedDHX, dhx, mining_power, mxc, optimizeResult, mPowerPerDHX }> = new BehaviorSubject({
    bondedDHX: null,
    dhx: null,
    mxc: null,
    mining_power: null,
    optimizeResult: false,
    mPowerPerDHX: null
  })
  DHXBonded$ = this.DHXBonded.asObservable()
  /* Forecast Results */
  forecast: Array<{ label: string, reward: RewardResume }>
  OPTIMAL_PERCENT_INDICATOR = 95 /* Consider as Optimal */
  refreshView = true
  constructor(private readonly service: HomeStore,
              private readonly cd: ChangeDetectorRef,
              private readonly loading: LoadingService) {
  }

  ngOnInit(){
    this.vm$.pipe(
      untilDestroyed(this),
      tap(vm => {
        if (this.refreshView && vm.dhxBondedInfo && vm.lastMining && vm.mxc && vm.dhx && vm.mPowerPerDHX) {
          this.DHXBonded.next({
            bondedDHX: +vm.dhxBondedInfo.dhxBonded,
            dhx: vm.dhx.buy,
            mxc: vm.mxc.buy,
            mining_power: +vm.lastMining.orgMiningPower,
            optimizeResult: vm.optimizeResult,
            mPowerPerDHX: vm.mPowerPerDHX
          })
          this.refreshView = false;
        }
      })
    ).subscribe()
    this.DHXBonded$.pipe(untilDestroyed(this)).subscribe(({ bondedDHX, mining_power, dhx, mxc, optimizeResult, mPowerPerDHX }) => {
      bondedDHX && this.processForecast({ bondedDHX, mining_power, dhx, mxc, optimizeResult, mPowerPerDHX })
    })
  }

  processForecast({ bondedDHX, mining_power, dhx, mxc, optimizeResult, mPowerPerDHX }) {
    this.forecast = []
    let nbDevice = 1
    let dhxBondedForecast = bondedDHX
    let miningPower = mining_power
    /* Forecast on the next 12 months */
    for (let i = 1; i < 12; i++) {
      const label = moment().add(i * 30, 'days').format('MMM Do YY')
      /* Get Reward for each Day */
      const rewardsResult = []
      for (let d = 0; d < 30; d++) {
        /* Reward of the day */
        const reward = this.getReward({ bondedDHX: dhxBondedForecast, mining_power: miningPower, mPowerPerDHX })
        rewardsResult.push(reward)
        /* 7 Days of cooling Of */
        if (d >= 6) {
          dhxBondedForecast += +rewardsResult[d - 6].expected_earning
        }
      }
      const totalDHXEarned = rewardsResult.map(res => +res.expected_earning).reduce((a, b) => a + b, 0)
      const optimalPercentAvg = rewardsResult.map(res => +res.optimal_percent).reduce((a, b) => (a + b)) / rewardsResult.length
      const optimal_mining = rewardsResult.map(res => +res.daily_mining_limit).reduce((a, b) => a + b, 0)
      /* User Request for Optimization */
      let rewardResume
      if (optimizeResult && optimalPercentAvg < this.OPTIMAL_PERCENT_INDICATOR) {
        /* Not Optimal */
        const dhxToReinvest = totalDHXEarned
        const dhxToUsd = dhxToReinvest * dhx
        const usdtToMxc = dhxToUsd / mxc
        /* Increase the mPower + Boost 40% from Stacking */
        let mPowerIncrease = usdtToMxc + (usdtToMxc * 40 / 100)
        if (nbDevice >= 1 && miningPower < 1000000) {
          /* Double the power under a million if at least one device */
          mPowerIncrease *= 2
        }
        miningPower += mPowerIncrease
        /* Decrease the DHX Bonded */
        dhxBondedForecast -= dhxToReinvest
        /* Alert Message to buy more device */
        let alert
        if (miningPower >= (nbDevice * 1000000)) {
          alert = `Buying a new m2 pro miner would double your mPower to ${this.nFormatter(miningPower * 2, 2)}`
        }
        rewardResume = {
          to_sell: true,
          alert,
          reinvestment: parseFloat(dhxToReinvest.toString()).toFixed(2),
          reinvestment_usd: this.nFormatter(dhxToUsd, 2),
          reinvestment_mxc: this.nFormatter(usdtToMxc, 2),
          miningPower: this.nFormatter(miningPower, 1),
        }
      } else {
        rewardResume = {
          thirty_day_dhx: parseFloat(`${totalDHXEarned}`).toFixed(2),
          thirty_day_usd: parseFloat(`${+totalDHXEarned * dhx}`).toFixed(2),
          optimal_percent: +parseFloat(optimalPercentAvg.toString()).toFixed(1),
          dhxBonded: +parseFloat(dhxBondedForecast.toString()).toFixed(2),
          reinvestment: parseFloat((optimal_mining - totalDHXEarned).toString()).toFixed(2),
          optimal_percent_indicator: this.OPTIMAL_PERCENT_INDICATOR,
        }
      }
      this.forecast.push({ label, reward: rewardResume })
      // console.log(this.forecast)
    }
  }

  getReward({ bondedDHX, mining_power, mPowerPerDHX }): RewardItem {
    const mPowerNeeded = bondedDHX * mPowerPerDHX
    let optimal_percent = (mining_power / mPowerNeeded) * 100
    optimal_percent = optimal_percent >= 100 ? 100 : optimal_percent
    const daily_mining_limit = +(bondedDHX / 70)
    const expected_earning = optimal_percent * (daily_mining_limit / 100)
    return {
      daily_mining_limit: parseFloat(daily_mining_limit.toString()).toFixed(2),
      expected_earning: parseFloat(expected_earning.toString()).toFixed(2),
      optimal_percent: parseFloat(optimal_percent.toString()).toFixed(2),
    }
  }

  nFormatter(num, digits) {
    const si = [
      { value: 1, symbol: '' },
      { value: 1E3, symbol: 'k' },
      { value: 1E6, symbol: 'M' },
      { value: 1E9, symbol: 'G' },
      { value: 1E12, symbol: 'T' },
      { value: 1E15, symbol: 'P' },
      { value: 1E18, symbol: 'E' },
    ]
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/
    let i
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, '$1') + si[i].symbol
  }

  async OptimizeEanings() {
    this.loading.start({ title: 'Optimizing your eanings', message: 'Please wait...' })
    /* Add a delay */
    await of().pipe(delay(1500)).toPromise()
    this.refreshView = true;
    this.service.optimizeResult()
    this.loading.close()
  }

  reset() {
    this.refreshView = true;
    this.service.resetOptimizeResult()
  }
}
