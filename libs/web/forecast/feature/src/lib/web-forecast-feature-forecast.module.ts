import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ForecastComponent } from './forecast.component'
import { RouterModule } from '@angular/router'
import { SharedUiPageModule } from '@simulator/shared/ui/page'
import { SharedUiTilesModule } from '@simulator/shared/ui/tiles'
import { WebForecastUiTimelineItemModule } from '@simulator/web/forecast/ui/timeline-item'

@NgModule({
  imports: [
    CommonModule,
    SharedUiPageModule,
    SharedUiTilesModule,
    WebForecastUiTimelineItemModule,
    RouterModule.forChild([
      {
        path: '',
        component: ForecastComponent,
      },
    ]),
  ],
  declarations: [ForecastComponent],
})
export class WebForecastFeatureForecastModule {}
