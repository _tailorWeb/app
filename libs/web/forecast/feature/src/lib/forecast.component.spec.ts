import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, mockProvider, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { of } from 'rxjs';

import { UiStore } from '@simulator/shared/data-access/ui-store'
import { LoadingService } from '@simulator/shared/ui/loading'
import { ForecastComponent } from './forecast.component'

describe('ForecastComponent', () => {
  let spectator: Spectator<ForecastComponent>;
  let mockedUiStore: SpyObject<UiStore>;

  const createComponent = createComponentFactory({
    component: ForecastComponent,
    imports: [RouterTestingModule],
    providers: [
      mockProvider(UiStore, {
        navItems$: of([])
      }),
      mockProvider(LoadingService, {
        start: null
      })
    ],
    shallow: true
  });

  beforeEach(() => {
    spectator = createComponent();
    mockedUiStore = spectator.inject(UiStore);
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
    // expect(spectator.component.navItems$).toBe(mockedUiStore.navItems$)
  });
});
