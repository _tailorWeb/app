import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { TimelineItemComponent } from './timeline-item.component'


describe('TimelineItemComponent', () => {
  let spectator: Spectator<TimelineItemComponent>;

  const createComponent = createComponentFactory({
    component: TimelineItemComponent,
    imports: [RouterTestingModule],
    shallow: true
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
