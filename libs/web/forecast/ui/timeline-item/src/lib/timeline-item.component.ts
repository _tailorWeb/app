import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import { RewardResume } from '../../../../feature/src/lib/forecast.component'

@Component({
  selector: 'simulator-timeline-item',
  templateUrl: './timeline-item.component.html',
  styles: [
    `
        :host {
            display: block;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineItemComponent implements OnInit {
  @Input() item: { label: string, reward: RewardResume }

  constructor() {
  }

  ngOnInit(): void {
  }

}
