import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { TimelineItemComponent } from './timeline-item.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { WebForecastUiTimelineButtonModule } from '@simulator/web/forecast/ui/timeline-button'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule,
    WebForecastUiTimelineButtonModule,
    SharedUiAlertModule
  ],
  declarations: [
    TimelineItemComponent
  ],
  exports: [
    TimelineItemComponent
  ],
})
export class WebForecastUiTimelineItemModule {}
