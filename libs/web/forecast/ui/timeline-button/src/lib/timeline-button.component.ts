import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-timeline-button',
  templateUrl: './timeline-button.component.html',
  styles: [
    `
        :host {
            display: inline-block;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineButtonComponent {
  @Input() optimal_percent: number
  @Input() optimal_percent_indicator: number
  @Input() dhxBonded: number
  @Input() reinvestment: string
  @Input() mining_power: string
  @Input() boost: string
}
