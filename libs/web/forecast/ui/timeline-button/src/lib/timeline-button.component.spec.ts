import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { TimelineButtonComponent } from './timeline-button.component'


describe('TimelineButtonComponent', () => {
  let spectator: Spectator<TimelineButtonComponent>;

  const createComponent = createComponentFactory({
    component: TimelineButtonComponent,
    imports: [RouterTestingModule],
    shallow: true
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
