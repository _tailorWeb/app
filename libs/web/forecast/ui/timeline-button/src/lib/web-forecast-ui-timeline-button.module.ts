import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { TimelineButtonComponent } from './timeline-button.component'

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TimelineButtonComponent
  ],
  exports: [
    TimelineButtonComponent
  ],
})
export class WebForecastUiTimelineButtonModule {}
