import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';

import { FaqComponent } from './faq.component'

describe('FaqComponent', () => {
  let spectator: Spectator<FaqComponent>;

  const createComponent = createComponentFactory({
    component: FaqComponent,
    imports: [RouterTestingModule],
    providers: [
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
