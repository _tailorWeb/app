import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FaqComponent } from './faq.component'
import { RouterModule } from '@angular/router'
import { SharedUiPageModule } from '@simulator/shared/ui/page'
import { WebFaqUiItemModule } from '@simulator/web/faq/ui/faq-item'
import { WebFaqUiCategoryModule } from '@simulator/web/faq/ui/faq-category'

@NgModule({
  imports: [
    CommonModule,
    SharedUiPageModule,
    WebFaqUiItemModule,
    WebFaqUiCategoryModule,
    RouterModule.forChild([
      {
        path: '',
        component: FaqComponent,
      },
    ]),
  ],
  declarations: [
    FaqComponent,
  ],
})
export class WebFaqFeatureModule {
}
