import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'simulator-faq',
  templateUrl: './faq.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FaqComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
