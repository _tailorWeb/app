import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { FaqCategoryComponent } from './faq-category.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule
  ],
  declarations: [
    FaqCategoryComponent
  ],
  exports: [
    FaqCategoryComponent
  ],
})
export class WebFaqUiCategoryModule {}
