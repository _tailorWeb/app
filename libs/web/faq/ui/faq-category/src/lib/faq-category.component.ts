import { Component, ChangeDetectionStrategy, Input } from '@angular/core'
import { UiIcon } from '@simulator/shared/ui/icon'

@Component({
  selector: 'simulator-faq-category',
  templateUrl: './faq-category.component.html',
  styles: [
    `
        :host {
            display: block;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FaqCategoryComponent{
  @Input() category: string;
  @Input() icon: UiIcon;

}
