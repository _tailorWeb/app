import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';

import { FaqCategoryComponent } from './faq-category.component'

describe('FaqCategoryComponent', () => {
  let spectator: Spectator<FaqCategoryComponent>;

  const createComponent = createComponentFactory({
    component: FaqCategoryComponent,
    imports: [RouterTestingModule],
    providers: [
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
