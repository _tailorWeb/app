import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { FaqItemComponent } from './faq-item.component'

@NgModule({
  imports: [CommonModule],
  declarations: [
    FaqItemComponent
  ],
  exports: [
    FaqItemComponent
  ],
})
export class WebFaqUiItemModule {}
