import { Component, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-faq-item',
  templateUrl: './faq-item.component.html',
  styles: [
    `
        :host {
            display: block;
        }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemComponent {
  @Input() title: string
  @Input() content: string

}
