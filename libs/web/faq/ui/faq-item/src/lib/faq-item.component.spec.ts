import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';

import { FaqItemComponent } from './faq-item.component'

describe('FaqItemComponent', () => {
  let spectator: Spectator<FaqItemComponent>;

  const createComponent = createComponentFactory({
    component: FaqItemComponent,
    imports: [RouterTestingModule],
    providers: [
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
