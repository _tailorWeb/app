import { Injectable } from '@angular/core'
import { ImmerComponentStore } from 'ngrx-immer/component-store'
import { catchError, switchMap, tap } from 'rxjs/operators'
import {
  DHXHistoryDto,
  SymbolsDto,
  DhxMiningGraphInterface, TxHistory,
} from '@simulator/shared/data-access/models'
import { DhxService, MxcService, SymbolsService } from '@simulator/web/home/data-access/services'
import { TransactionsDto, TxHistoryDetail } from '@simulator/shared/data-access/models'
import { forkJoin, Observable, of } from 'rxjs'
import * as moment from 'moment'

export const defaultDate: DateRange = { start: moment().subtract('1', 'week').toDate(), end: moment().toDate() }

export interface DateRange {
  start: Date;
  end: Date;
}

interface StatsState {
  mxc: null | SymbolsDto,
  dhx: null | SymbolsDto,
  date: DateRange,
  isLoading: boolean,
  dhxMiningHistory: DHXHistoryDto,
  dhxTransactions: TransactionsDto,
  mxcTransactions: TransactionsDto,
  allTransactions: TxHistory[],
  exportedDataDHX: DhxMiningGraphInterface[],
  exportedDataMXC: DhxMiningGraphInterface[],
  error: null | string
}


@Injectable({ providedIn: 'root' })
export class StatsStore extends ImmerComponentStore<StatsState> {
  constructor(private readonly symbolsService: SymbolsService,
              private readonly dhxService: DhxService,
              private readonly mxcService: MxcService,
  ) {
    super({
      mxc: null,
      dhx: null,
      date: defaultDate,
      isLoading: true,
      dhxMiningHistory: null,
      dhxTransactions: null,
      mxcTransactions: null,
      allTransactions: [],
      exportedDataDHX: [],
      exportedDataMXC: [],
      error: null,
    })
    this.initializeDateEffect(defaultDate)
  }

  readonly vm$ = this.select(({
                                mxc, dhx,
                                error, allTransactions,
                                dhxMiningHistory, dhxTransactions,
                                mxcTransactions, exportedDataDHX,
                                exportedDataMXC, date,
                                isLoading,
                              }) => ({
    mxc,
    dhx,
    date,
    isLoading,
    dhxMiningHistory,
    dhxTransactions,
    mxcTransactions,
    exportedDataDHX,
    exportedDataMXC,
    allTransactions,
    error,
  }))

  readonly isLoading$ = this.select((s) => s.isLoading)

  readonly initializeDateEffect = this.effect((date$: Observable<DateRange>) =>
    date$.pipe(
      tap(() => this.startLoading()),
      tap(() => this.resetTransactions()),
      switchMap((date) => {
        return forkJoin([
          this.symbolsService.getMXC().pipe(tap(mxc => this.patchState({ mxc }))),
          this.symbolsService.getDHX().pipe(tap(dhx => this.patchState({ dhx }))),
          this.dhxService.getHistory(
            moment(date.start).format('YYYY-MM-DDTHH:mm:ss'),
            moment(date.end).format('YYYY-MM-DDTHH:mm:ss'),
          ).pipe(
            tap(dhxMiningHistory => {
              this.patchState({ dhxMiningHistory, exportedDataDHX: [...dhxMiningHistory.dhxMining[0].series] })
              this.updateExportedDhxData(dhxMiningHistory.dhxMining[0].series)
            })),
          this.dhxService.getTransactions(moment(date.start).format('YYYY-MM-DDTHH:mm:ss'), moment(date.end).format('YYYY-MM-DDTHH:mm:ss')).pipe(
            tap(dhxTransactions => {
              this.patchState({ dhxTransactions })
              const allTransactions = this.formatTransaction(dhxTransactions, 'DHX')
              this.updateAllTransactions(allTransactions)
            }),
          ),
          this.mxcService.getTransactions(
            moment(date.start).format('YYYY-MM-DDTHH:mm:ss'),
            moment(date.end).format('YYYY-MM-DDTHH:mm:ss'),
          ).pipe(
            tap(mxcTransactions => {
              this.patchState({ mxcTransactions })
              this.updateExportedMxcData(mxcTransactions.minings[0].series)
              const allTransactions = this.formatTransaction(mxcTransactions, 'MXC')
              this.updateAllTransactions(allTransactions)
            })),
        ])
      }),
      tap(() => this.endLoading()),
      catchError(err => {
        const error = err.message
        this.patchState({ error, isLoading: false })
        return of(error)
      }),
    ),
  )

  private formatTransaction(dhxTransactions, label) {
    return dhxTransactions.tx.map(el => {
      const detailsJson = JSON.parse(el.detailsJson as string) as TxHistoryDetail
      let timestamp = el.timestamp
      if (el.detailsJson && el.detailsJson.LockTill && el.detailsJson.LockTill.includes('0001-01-01T00:00:00Z')) {
        delete detailsJson.LockTill
      }
      if (detailsJson) {
        timestamp = detailsJson.TxApprovedTime || detailsJson.StartTime
      }
      return {
        ...el, title: label, amount: parseFloat(el.amount).toFixed(2),
        timestamp, detailsJson,
      }
    }).sort((a, b) => +new Date(b.timestamp ? b.timestamp : b.detailsJson.Created) - +new Date(a.timestamp ? a.timestamp : a.detailsJson.Created))
  }

  readonly updateDate = this.updater((state, date: DateRange) => {
    state.date = date
  })

  readonly startLoading = this.updater((state) => {
    state.isLoading = true
  })

  readonly resetTransactions = this.updater((state) => {
    state.allTransactions = []
  })

  readonly endLoading = this.updater((state) => {
    state.isLoading = false
  })

  readonly updateAllTransactions = this.updater((state, series: TxHistory[]) => {
    state.allTransactions = [...state.allTransactions, ...series]
  })

  readonly updateExportedDhxData = this.updater((state, series: DhxMiningGraphInterface[]) => {
    state.exportedDataDHX = [...state.exportedDataDHX, ...series]
  })

  readonly updateExportedMxcData = this.updater((state, series: DhxMiningGraphInterface[]) => {
    state.exportedDataMXC = [...state.exportedDataMXC, ...series]
  })

}
