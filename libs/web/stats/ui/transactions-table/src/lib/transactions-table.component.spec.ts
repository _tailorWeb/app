import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { TransactionsTableComponent } from './transactions-table.component'


describe('TransactionsTableComponent', () => {
  let spectator: Spectator<TransactionsTableComponent>
  const createComponent = createComponentFactory(TransactionsTableComponent)

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
