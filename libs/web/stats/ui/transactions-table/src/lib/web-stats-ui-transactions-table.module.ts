import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TransactionsTableComponent } from './transactions-table.component'
import { MomentModule } from 'ngx-moment'

@NgModule({
  imports: [
    CommonModule,
    MomentModule
  ],
  declarations: [
    TransactionsTableComponent,
  ],
  exports: [TransactionsTableComponent],
})
export class WebStatsUiTransactionsTableModule {
}
