import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import {
  TxHistory,
} from '@simulator/shared/data-access/models'

@Component({
  selector: 'simulator-transactions-table',
  templateUrl: './transactions-table.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionsTableComponent {
  @Input() allTransactions: TxHistory[]

  constructor() {
  }

  convertLabelType(type) {
    switch (type) {
      case 'STAKING':
        return 'Stacking'
      case 'UNSTAKING':
        return 'Un-Stacking'
      case 'TOPUP':
        return 'Into my Wallet'
      case 'DHX_BONDING':
        return 'Bonding DHX'
      case 'DHXLOCK':
        return 'Locking for DHX'
      default:
        return type
    }
  }

}
