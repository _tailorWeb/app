import { Component, OnInit, ChangeDetectionStrategy, HostListener, ViewChild, OnDestroy } from '@angular/core'
import * as moment from 'moment'
import { NgxDatesPickerComponent } from 'ngx-dates-picker'
import {
  SymbolsDto,
} from '@simulator/shared/data-access/models'
import * as FileSaver from 'file-saver'
import * as XLSX from 'xlsx'
import { StatsStore, defaultDate, DateRange } from '@simulator/web/stats/data-access/stats-store'
import { LoadingService } from '@simulator/shared/ui/loading'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
const EXCEL_EXTENSION = '.xlsx'

@UntilDestroy()
@Component({
  selector: 'simulator-stats',
  templateUrl: './stats.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatsComponent implements OnInit, OnDestroy {
  vm$ = this.service.vm$

  minDate = moment().subtract('1', 'week').toDate()
  maxDate = moment().toDate()
  date: DateRange = {...defaultDate}

  @ViewChild('pickerFrom') pickerFrom
  lastPickerStatus = false

  @HostListener('document:click', ['$event']) onBlur(e: MouseEvent) {
    if (!this.pickerFrom.isOpened && this.lastPickerStatus !== this.pickerFrom.isOpened) {
      this.service.updateDate(this.date)
      this.service.initializeDateEffect(this.date)
    }
    this.lastPickerStatus = this.pickerFrom.isOpened
  }

  constructor(private readonly service: StatsStore,
              private readonly loading: LoadingService) {
  }

  ngOnInit() {
    this.service.isLoading$.pipe(untilDestroyed(this)).subscribe((val)=>{
      if (val) {
        this.loading.start({title: 'Loading', message: 'Please wait while loading your data...'})
      } else {
        this.loading.close()
      }
    })
  }

  openPicker(e, pickerFrom: NgxDatesPickerComponent) {
    e.stopPropagation()
    pickerFrom.isOpened = !pickerFrom.isOpened
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] }
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' })
    this.saveAsExcelFile(excelBuffer, excelFileName)
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE })
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION)
  }

  exportDataDHXHandler(dhxPrice: SymbolsDto, exportedDataDHX) {
    if (exportedDataDHX.length === 0) return
    const results = exportedDataDHX.map(el => {
      return {
        Title: el.name,
        Value: el.value,
        Price: parseFloat((+el.value * dhxPrice.buy).toString()).toFixed(2) + ' $',
      }
    })
    this.exportAsExcelFile(results, `dhx`)
  }

  exportDataMXCHandler(mxcPrice: SymbolsDto, exportedDataMXC) {
    if (exportedDataMXC.length === 0) return
    const results = exportedDataMXC.map(el => {
      return {
        Title: el.name,
        Value: el.value,
        Price: parseFloat((+el.value * mxcPrice.buy).toString()).toFixed(2) + ' $',
      }
    })
    this.exportAsExcelFile(results, `mxc`)
  }

  ngOnDestroy() {
    this.loading.close()
  }
}
