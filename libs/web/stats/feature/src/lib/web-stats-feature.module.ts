import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { StatsComponent } from './stats.component'
import { SharedUiPageModule } from '@simulator/shared/ui/page'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { RouterModule } from '@angular/router'
import { SharedUiTilesModule } from '@simulator/shared/ui/tiles'
import { FormsModule } from '@angular/forms'
import { NgxDatesPickerModule } from 'ngx-dates-picker'
import { MomentModule } from 'ngx-moment'
import { WebStatsUiTransactionsTableModule } from '@simulator/web/stats/ui/transactions-table'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    SharedUiPageModule,
    NgxChartsModule,
    SharedUiTilesModule,
    RouterModule.forChild([
      {
        path: '',
        component: StatsComponent,
      },
    ]),
    FormsModule,
    NgxDatesPickerModule,
    MomentModule,
    SharedUiAlertModule,
    WebStatsUiTransactionsTableModule,
  ],
  declarations: [StatsComponent],
})
export class WebStatsFeatureModule {}
