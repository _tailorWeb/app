import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { SignupComponent } from './signup.component'


describe('SignupComponent', () => {
  let spectator: Spectator<SignupComponent>

  const createComponent = createComponentFactory({
    component: SignupComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
