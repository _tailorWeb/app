import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup.component'
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
        {
          path: '',
          component: SignupComponent,
        },
      ],
    ),
    ReactiveFormsModule,
    SharedUiIconModule,
    SharedUiAlertModule,
  ],
  declarations: [
    SignupComponent
  ],
})
export class WebSessionFeatureSignupModule {}
