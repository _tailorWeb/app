import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Observable, Subject } from 'rxjs'
import { Router } from '@angular/router'
import { AuthService } from '@simulator/shared/data-access/services'

@Component({
  selector: 'simulator-signup',
  templateUrl: './signup.component.html',
  styles: [
    `#submit {
        background: #4564ff
    }`,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignupComponent implements OnInit {
  userForm: FormGroup
  error = new Subject<string>()
  error$: Observable<string> = this.error.asObservable()

  constructor(private readonly fb: FormBuilder,
              private readonly router: Router,
              private readonly authService: AuthService) {
  }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    })
  }

  async register() {
    if (this.userForm.get('password').value !== this.userForm.get('confirmPassword').value) {
      return this.error.next('Passwords does not match')
    }
    this.authService.register(this.userForm.value)
      .then(() => {
        return this.router.navigate(['/'])
      })
      .catch(e => {
        this.error.next(e)
      })
  }

}
