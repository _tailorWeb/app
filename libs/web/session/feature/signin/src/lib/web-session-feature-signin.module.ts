import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SigninComponent } from './signin.component'
import { RouterModule } from '@angular/router'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
        {
          path: '',
          component: SigninComponent,
        },
      ],
    ),
    SharedUiIconModule,
    SharedUiAlertModule,
    ReactiveFormsModule,
  ],
  declarations: [
    SigninComponent,
  ],
})
export class WebSessionFeatureSigninModule {
}
