import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AuthService } from '@simulator/shared/data-access/services'
import { Router } from '@angular/router'
import { Observable, Subject } from 'rxjs'

@Component({
  selector: 'simulator-signin',
  templateUrl: './signin.component.html',
  styles: [
    `#submit {
        background: #FF4564
    }`,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SigninComponent implements OnInit {
  userForm: FormGroup
  error = new Subject<string>()
  error$: Observable<string> = this.error.asObservable()

  constructor(private readonly fb: FormBuilder,
              private readonly router: Router,
              private readonly authService: AuthService) {
  }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    })
  }

  async login() {
    this.authService.login(this.userForm.value)
      .then(() => {
        return this.router.navigate([''])
      })
      .catch(e => {
        this.error.next(e)
      })
  }

  connectSocial(provider: 'google' | 'facebook' | 'twitter') {
    this.authService.signInProvider(provider)
      .then(() => {
        return this.router.navigate([''])
      })
      .catch(e => {
        this.error.next(e)
      })
  }
}
