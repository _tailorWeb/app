import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { SigninComponent } from './signin.component'


describe('SigninComponent', () => {
  let spectator: Spectator<SigninComponent>

  const createComponent = createComponentFactory({
    component: SigninComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
