import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ForgetPasswordComponent } from './forget-password.component'
import { ReactiveFormsModule } from '@angular/forms'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'
import { RouterModule } from '@angular/router'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
        {
          path: '',
          component: ForgetPasswordComponent,
        },
      ],
    ),
    ReactiveFormsModule,
    SharedUiIconModule,
    SharedUiAlertModule,
  ],
  declarations: [
    ForgetPasswordComponent,
  ],
})
export class WebSessionFeatureForgetPasswordModule {
}
