import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { ForgetPasswordComponent } from './forget-password.component'


describe('ForgetPasswordComponent', () => {
  let spectator: Spectator<ForgetPasswordComponent>

  const createComponent = createComponentFactory({
    component: ForgetPasswordComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })
})
