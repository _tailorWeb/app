import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Observable, Subject } from 'rxjs'
import { Router } from '@angular/router'
import { AuthService } from '@simulator/shared/data-access/services'
import { LoadingService } from '@simulator/shared/ui/loading'

@Component({
  selector: 'simulator-forget-password',
  templateUrl: './forget-password.component.html',
  styles: [
    `#submit {
        background: #4564ff
    }`,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgetPasswordComponent implements OnInit {
  forgetForm: FormGroup
  error = new Subject<string>()
  error$: Observable<string> = this.error.asObservable()

  constructor(private readonly fb: FormBuilder,
              private readonly router: Router,
              private readonly loading: LoadingService,
              private readonly authService: AuthService) {
  }

  ngOnInit(): void {
    this.forgetForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    })
  }

  submit() {
    this.loading.start({ title: 'Forget Password', message: 'Please check your inbox...' })
    this.authService.resetPwd(this.forgetForm.get('email').value)
      .then(() => {
        setTimeout(() => {
          this.loading.close()
          return this.router.navigate(['/'])
        }, 2000)
      })
      .catch(e => {
        setTimeout(() => {
          this.loading.close()
          this.error.next(e)
        }, 1000)
      })
  }

}
