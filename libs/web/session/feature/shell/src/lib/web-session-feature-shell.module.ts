import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'sign-in',
        loadChildren: () =>
          import('@simulator/web/session/feature/signin').then(
            (m) => m.WebSessionFeatureSigninModule,
          ),
      },
      {
        path: 'forget-password',
        loadChildren: () =>
          import('@simulator/web/session/feature/forget-password').then(
            (m) => m.WebSessionFeatureForgetPasswordModule,
          ),
      },
      {
        path: 'register',
        loadChildren: () =>
          import('@simulator/web/session/feature/signup').then(
            (m) => m.WebSessionFeatureSignupModule,
          ),
      },
      {
        path: 'not-found',
        loadChildren: () =>
          import('@simulator/web/session/feature/not-found').then(
            (m) => m.WebSessionFeatureNotFoundModule,
          ),
      },
    ]),
  ],
})
export class WebSessionFeatureShellModule {}
