import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule } from '@angular/router'
import { simulatorShellRoutes } from './web-shell.routes'
import { SharedUiHeaderModule } from '@simulator/shared/ui/header'
import { SharedUiFooterModule } from '@simulator/shared/ui/footer'
import { SharedUiSidenavModule } from '@simulator/shared/ui/sidenav'
import { LayoutComponent } from './layout/main/layout.component'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert';
import { AuthComponent } from './layout/auth/auth.component'

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forRoot(simulatorShellRoutes),
    SharedUiHeaderModule,
    SharedUiFooterModule,
    SharedUiSidenavModule,
    SharedUiAlertModule,
  ],
  declarations: [
    LayoutComponent,
    AuthComponent
  ],
  exports: [RouterModule],
})
export class WebShellModule {}
