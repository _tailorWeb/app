import { Component, ChangeDetectionStrategy } from '@angular/core'
import { UiStore } from '@simulator/shared/data-access/ui-store'
import { tap } from 'rxjs/operators'
import { MessagingService, AuthService } from '@simulator/shared/data-access/services'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'

@UntilDestroy()
@Component({
  templateUrl: './layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutComponent {
  vm$ = this.service.vm$

  constructor(
    private readonly service: UiStore,
    private readonly authService: AuthService,
    private readonly msg: MessagingService,
  ) {
    this.authService.user$.pipe(
      untilDestroyed(this),
      tap(user => {
        if (user && 'serviceWorker' in navigator) {
          this.msg.getPermission(user).subscribe()
          this.msg.receiveMessages()
        }
      }),
    ).subscribe()
  }

}
