import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'

@Component({
  selector: 'simulator-auth',
  templateUrl: './auth.component.html',
  styles: [
    `
        section {
            color: #0A2463;
            background: #F2F5FA;
        }

    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
