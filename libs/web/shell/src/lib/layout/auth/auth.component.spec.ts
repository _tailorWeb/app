import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { AuthComponent } from './auth.component'


describe('AuthLayoutComponent', () => {
  let spectator: Spectator<AuthComponent>

  const createComponent = createComponentFactory({
    component: AuthComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })

  it('should contain header, footer, and router-outlet', () => {
    expect(spectator.query('router-outlet')).toBeTruthy()
  })
})
