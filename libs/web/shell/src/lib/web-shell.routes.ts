import { Routes } from '@angular/router'
import { LayoutComponent } from './layout/main/layout.component'
import { AuthComponent } from './layout/auth/auth.component'
import { AuthGuard } from '@simulator/shared/data-access/services'

export const simulatorShellRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      {
        path: 'home',
        loadChildren: () =>
          import('@simulator/web/home/feature').then((m) => m.WebHomeFeatureModule),
      },
      {
        path: 'stats',
        loadChildren: () =>
          import('@simulator/web/stats/feature').then((m) => m.WebStatsFeatureModule),
      },
      {
        path: 'forecast',
        loadChildren: () =>
          import('@simulator/web/forecast/feature').then((m) => m.WebForecastFeatureForecastModule),
      },
      {
        path: 'suggestion',
        loadChildren: () =>
          import('@simulator/web/suggestion/feature').then((m) => m.WebSuggestionFeatureModule),
      },
      {
        path: 'faq',
        loadChildren: () =>
          import('@simulator/web/faq/feature').then((m) => m.WebFaqFeatureModule),
      },
    ],
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () =>
          import('@simulator/web/session/feature/shell').then((m) => m.WebSessionFeatureShellModule),
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'auth/404'
  },
]
