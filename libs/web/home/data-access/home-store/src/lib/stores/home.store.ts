import { Injectable } from '@angular/core'
import { ImmerComponentStore } from 'ngrx-immer/component-store'
import { catchError, switchMap, tap } from 'rxjs/operators'
import {
  DHXBondedDto, DHXHistoryDto,
  DHXLastMiningDto, MXCGlobalStackDto,
  SymbolsDto,
  DeviceDto, MXCWalletDto, TransactionsDto, ProfileDto,
} from '@simulator/shared/data-access/models'
import { SyncService } from '@simulator/shared/ui/sync'
import {
  DhxService,
  MxcService,
  ProfileService,
  SymbolsService,
  DeviceService,
} from '@simulator/web/home/data-access/services'
import { forkJoin, of } from 'rxjs'
import * as moment from 'moment'

interface UiState {
  device: null | DeviceDto,
  mxc: null | SymbolsDto,
  dhx: null | SymbolsDto,
  error: null | string
  profile: null | ProfileDto
  dhxBondedInfo: null | DHXBondedDto
  dhxMiningHistory: null | DHXHistoryDto
  lastMining: null | DHXLastMiningDto
  lastMiningMXC: null | TransactionsDto
  mxcStacking: null | MXCGlobalStackDto
  mxcWallet: null | MXCWalletDto
  optimizeResult: boolean
  mPowerPerDHX: null | number
}

/* Time in ms */
const REFRESH_SYMBOLS = 30000

@Injectable({ providedIn: 'root' })
export class HomeStore extends ImmerComponentStore<UiState> {

  constructor(private readonly symbolsService: SymbolsService,
              private readonly syncService: SyncService,
              private readonly profileService: ProfileService,
              private readonly dhxService: DhxService,
              private readonly mxcService: MxcService,
              private readonly deviceService: DeviceService,
  ) {
    super({
      device: null,
      mxc: null,
      dhx: null,
      error: null,
      profile: null,
      dhxBondedInfo: null,
      dhxMiningHistory: null,
      lastMining: null,
      lastMiningMXC: null,
      mxcStacking: null,
      mxcWallet: null,
      optimizeResult: false,
      mPowerPerDHX: null,
    })
    this.initializeEffect()
    /* Refresh Symbols (MXC / DHX ...) every 10 sec */
    setInterval(() => {
      this.refreshSymbolsEffects()
    }, REFRESH_SYMBOLS)
  }

  readonly vm$ = this.select(({
                                mxc, dhx,
                                dhxBondedInfo, optimizeResult,
                                lastMining, mxcStacking,
                                dhxMiningHistory, error,
                                device, mxcWallet,
                                lastMiningMXC, profile,
                                mPowerPerDHX
                              }) => ({
    mxc,
    dhx,
    error,
    profile,
    dhxBondedInfo,
    dhxMiningHistory,
    lastMining,
    mxcStacking,
    mxcWallet,
    device,
    lastMiningMXC,
    optimizeResult,
    mPowerPerDHX
  }))


  readonly initializeEffect = this.effect(($) =>
    $.pipe(
      switchMap(_ => {
        return forkJoin([
          this.symbolsService.getMXC().pipe(tap(mxc => this.patchState({ mxc }))),
          this.symbolsService.getDHX().pipe(tap(dhx => this.patchState({ dhx }))),
          this.profileService.getProfile().pipe(
            tap(profile => this.patchState({ profile })),
            switchMap(() => {
              return forkJoin([
                this.mxcService.getStacking().pipe(tap(mxcStacking => this.patchState({ mxcStacking }))),
                this.mxcService.getWallet().pipe(tap(mxcWallet => this.patchState({ mxcWallet }))),
                this.mxcService.getTransactions(
                  moment().startOf('day').format('YYYY-MM-DDTHH:mm:ss'),
                  moment().endOf('day').format('YYYY-MM-DDTHH:mm:ss'),
                ).pipe(tap(lastMiningMXC => this.patchState({ lastMiningMXC }))),
                this.dhxService.getHistory().pipe(tap(dhxMiningHistory => this.patchState({ dhxMiningHistory }))),
                this.dhxService.getBondInfo().pipe(tap(dhxBondedInfo => this.patchState({ dhxBondedInfo }))),
                this.dhxService.getLastMining().pipe(tap(lastMining => this.patchState({ lastMining }))),
                this.dhxService.getmPowerPerDHX().pipe(tap(mPowerPerDHX => this.patchState({ mPowerPerDHX }))),
                this.deviceService.getDevices().pipe(tap(device => this.patchState({ device }))),
              ])
            }),
          ),
        ])
      }),
      catchError(err => {
        console.log('err', err)
        const error = err.message
        this.patchState({ error })
        return of(error)
      }),
    ),
  )

  private readonly refreshSymbolsEffects = this.effect(($) =>
    $.pipe(
      switchMap(_ => {
        return forkJoin([
          this.symbolsService.getMXC().pipe(tap(mxc => this.patchState({ mxc }))),
          this.symbolsService.getDHX().pipe(tap(dhx => this.patchState({ dhx }))),
        ])
      }),
    ))

  readonly optimizeResult = this.updater((state) => {
    state.optimizeResult = true
  })

  readonly resetOptimizeResult = this.updater((state) => {
    state.optimizeResult = false
  })

}
