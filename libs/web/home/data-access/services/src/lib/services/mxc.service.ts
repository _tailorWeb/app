import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import type { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { MXCGlobalStackDto, MXCWalletDto, TransactionsDto } from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from '@simulator/shared/data-access/services'

@Injectable({ providedIn: 'root' })
export class MxcService {

  constructor(
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    private readonly httpClient: HttpClient) {
  }

  getStacking(): Observable<MXCGlobalStackDto> {
    return this.httpClient
      .get<any>(`${this.appConfig.baseURL}/mxc/stacking`)
  }

  getWallet(): Observable<MXCWalletDto> {
    return this.httpClient
      .get<MXCWalletDto>(`${this.appConfig.baseURL}/mxc/wallet`).pipe(
        map((wallet: MXCWalletDto) => {
          if (wallet) {
            wallet.balance = parseFloat(wallet.balance).toFixed(2)
            return wallet;
          }
        }))
  }

  getTransactions(from?: string, to?: string): Observable<TransactionsDto> {
    const params = (from && to) ? 'from=' + from + '&to=' + to : '';
    return this.httpClient
      .get<any>(`${this.appConfig.baseURL}/mxc/transactions?${params}`)
  }

}
