import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import type { Observable } from 'rxjs'
import {
  DHXBondedDto,
  DHXHistoryDto,
  DHXLastMiningDto,
  TransactionsDto,
} from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from '@simulator/shared/data-access/services'
import { map } from 'rxjs/operators'

@Injectable({ providedIn: 'root' })
export class DhxService {

  constructor(
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    private readonly httpClient: HttpClient) {
  }

  getBondInfo(): Observable<DHXBondedDto> {
    return this.httpClient
      .get<DHXBondedDto>(`${this.appConfig.baseURL}/dhx`)
  }

  getLastMining(): Observable<DHXLastMiningDto> {
    return this.httpClient
      .get<DHXLastMiningDto>(`${this.appConfig.baseURL}/dhx/last-mining`)
  }

  getHistory(from?: string, to?: string): Observable<DHXHistoryDto> {
    const params = (from && to) ? 'from=' + from + '&to=' + to : ''
    return this.httpClient
      .get<DHXHistoryDto>(`${this.appConfig.baseURL}/dhx/history?${params}`)
  }

  getTransactions(from?: string, to?: string): Observable<TransactionsDto> {
    const params = (from && to) ? 'from=' + from + '&to=' + to : ''
    return this.httpClient
      .get<TransactionsDto>(`${this.appConfig.baseURL}/dhx/transactions?${params}`)
  }

  getmPowerPerDHX(): Observable<number> {
    return this.httpClient
      .get<number>(`https://docs.google.com/spreadsheets/d/1NLkSf6T23dAS77gJ3_b-veXvlVQ1Z5Z3kg-AcxI43Zk/`,
        // @ts-ignore
        {responseType: 'text'})
      .pipe(
      map(res => {
        // @ts-ignore
        return +res.split('mPower per DHX</td><td class="s4" dir="ltr">')[1].split('</td>')[0].trim();
      }),
    )
  }

}
