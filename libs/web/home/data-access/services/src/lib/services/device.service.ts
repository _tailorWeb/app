import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import type { Observable } from 'rxjs'
import {
  DeviceDto, DeviceFrame,
} from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from '@simulator/shared/data-access/services'
import { map, switchMap, tap } from 'rxjs/operators'
import { forkJoin, of } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class DeviceService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    private readonly httpClient: HttpClient) {
  }

  getDevices(): Observable<DeviceDto> {
    return this.httpClient
      .get<DeviceDto>(`${this.appConfig.baseURL}/device`)
      .pipe(
        switchMap(devices => {
          return forkJoin([
            ...devices.result.map(device => this.getFrames(device.id)),
          ]).pipe(map((frames) => {
            devices.totalFrames = 0
            frames.map((frame: DeviceFrame[], i) => {
              devices.result[i].frames = frame
              devices.totalFrames += frame.map(el => el.rxPacketsReceived).reduce((a, b) => a + b, 0)
            })
            return devices
          }))
        }),
      )
  }

  getFrames(deviceId: string, from?: string, to?: string): Observable<DeviceFrame[]> {
    const params = (from && to) ? 'from=' + from + '&to=' + to : ''
    return this.httpClient
      .get<{ result: DeviceFrame[] }>(`${this.appConfig.baseURL}/device/frames/${deviceId}?${params}`).pipe(
        map(res => res.result),
      )
  }
}
