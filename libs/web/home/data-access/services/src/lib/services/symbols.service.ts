import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import type { Observable } from 'rxjs'
import { SymbolsDto } from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from '@simulator/shared/data-access/services'

@Injectable({ providedIn: 'root' })
export class SymbolsService {

  constructor(
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    private readonly httpClient: HttpClient) {
  }

  getMXC(): Observable<SymbolsDto> {
    return this.httpClient
      .get<SymbolsDto>(`${this.appConfig.baseURL}/symbols/mxc`)
  }

  getDHX(): Observable<SymbolsDto> {
    return this.httpClient
      .get<SymbolsDto>(`${this.appConfig.baseURL}/symbols/dhx`)
  }

}
