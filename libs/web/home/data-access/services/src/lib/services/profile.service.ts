import { HttpClient } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import type { Observable } from 'rxjs'
import { ProfileDto } from '@simulator/shared/data-access/models'
import { APP_CONFIG, AppConfig } from '@simulator/shared/data-access/services'

@Injectable({ providedIn: 'root' })
export class ProfileService {
  constructor(
    @Inject(APP_CONFIG) private appConfig: AppConfig,
    private readonly httpClient: HttpClient) {
  }

  getProfile(): Observable<ProfileDto> {
    return this.httpClient
      .get<ProfileDto>(`${this.appConfig.baseURL}/profile`)
  }

  getBranding(): Observable<{ logoPath: string }> {
    return this.httpClient
      .get<{ logoPath: string }>(`${this.appConfig.baseURL}/profile/branding`)
  }

}
