import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { ResultSimulatorComponent } from './result-simulator.component'


describe('ResultSimulatorComponent', () => {
  let spectator: Spectator<ResultSimulatorComponent>

  const createComponent = createComponentFactory({
    component: ResultSimulatorComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })

})
