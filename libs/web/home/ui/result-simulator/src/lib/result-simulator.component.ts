import { Component, ChangeDetectionStrategy, Input } from '@angular/core'
import { HelperService } from '@simulator/shared/ui/helper'

@Component({
  selector: 'simulator-result-simulator',
  templateUrl: './result-simulator.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultSimulatorComponent {
  @Input() label
  @Input() value
  @Input() subvalue
  @Input() showHelper = true
  @Input() icon

  constructor(private readonly helperService: HelperService) {
  }

  handleHelper(label: string) {
    switch (label) {
      case 'Today\'s Mining':
        return this.helperService.start({ title: label, message: `This value show you how much DHX you earned today` })
      case 'Daily mining Limit':
        return this.helperService.start({ title: label, message: `This value show you the DHX daily mining limit` })
      case 'Expecting earnings':
        return this.helperService.start({ title: label, message: `This value show you the expecting earnings at the current price` })
      case 'Optimal':
        return this.helperService.start({
          title: label,
          message: `This value show you if your are or not to your maximum earning`,
        })
    }
  }
}
