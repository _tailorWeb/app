import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { ResultSimulatorComponent } from './result-simulator.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'
import { SharedItemLoadModule } from '@simulator/shared/item-load'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule,
    SharedItemLoadModule
  ],
  declarations: [
    ResultSimulatorComponent
  ],
  exports: [
    ResultSimulatorComponent
  ],
})
export class WebHomeUiResultSimulatorModule {}
