import { RouterTestingModule } from '@angular/router/testing'
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest'
import { MarketIndicatorComponent } from './market-indicator.component'


describe('MarketIndicatorComponent', () => {
  let spectator: Spectator<MarketIndicatorComponent>

  const createComponent = createComponentFactory({
    component: MarketIndicatorComponent,
    imports: [RouterTestingModule],
    shallow: true,
  })

  beforeEach(() => (spectator = createComponent()))

  it('should create', () => {
    expect(spectator.component).toBeTruthy()
  })

})
