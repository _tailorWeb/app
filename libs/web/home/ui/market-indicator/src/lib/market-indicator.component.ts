import { Component, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-market-indicator',
  templateUrl: './market-indicator.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketIndicatorComponent {
  @Input() isBullish: boolean

}
