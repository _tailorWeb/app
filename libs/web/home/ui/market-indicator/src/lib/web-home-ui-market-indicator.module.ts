import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { MarketIndicatorComponent } from './market-indicator.component'

@NgModule({
  imports: [CommonModule],
  declarations: [
    MarketIndicatorComponent
  ],
  exports: [
    MarketIndicatorComponent
  ],
})
export class WebHomeUiMarketIndicatorModule {}
