import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RoseIndicatorComponent } from './rose-indicator.component'
import { SharedUiIconModule } from '@simulator/shared/ui/icon'

@NgModule({
  imports: [
    CommonModule,
    SharedUiIconModule,
  ],
  declarations: [
    RoseIndicatorComponent,
  ],
  exports: [
    RoseIndicatorComponent,
  ],
})
export class WebHomeUiRoseIndicatorModule {
}
