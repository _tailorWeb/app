import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { RoseIndicatorComponent } from './rose-indicator.component'


describe('RoseIndicatorComponent', () => {
  let spectator: Spectator<RoseIndicatorComponent>;

  const createComponent = createComponentFactory({
    component: RoseIndicatorComponent,
    imports: [RouterTestingModule],
    shallow: true
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
