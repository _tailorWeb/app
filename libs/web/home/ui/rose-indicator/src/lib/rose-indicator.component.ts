import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'

@Component({
  selector: 'simulator-rose-indicator',
  templateUrl: './rose-indicator.component.html',
  styles: [
    `
        :host {
            display: block;
        }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoseIndicatorComponent implements OnInit {
  @Input() rose: number;
  constructor() { }

  ngOnInit(): void {
  }

}
