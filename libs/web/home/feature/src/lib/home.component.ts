import { Component, ChangeDetectionStrategy } from '@angular/core'
import { HomeStore } from '@simulator/web/home/data-access/home-store'
import { DeviceDetectorService } from 'ngx-device-detector'

@Component({
  selector: 'simulator-home',
  templateUrl: './home.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  vm$ = this.service.vm$
  view
  formatPieChartValue = (val) => {
    return `${val} $`
  }

  constructor(
    private readonly service: HomeStore,
    private readonly deviceDetector: DeviceDetectorService,
  ) {
    this.view = this.deviceDetector.isMobile() ? null : [400, 300]
  }

}
