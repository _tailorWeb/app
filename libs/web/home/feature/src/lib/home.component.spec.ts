import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, mockProvider, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { of } from 'rxjs';

import { HomeComponent } from './home.component';
import { UiStore } from '@simulator/shared/data-access/ui-store'
import { LoadingService } from '@simulator/shared/ui/loading'

describe('HomeComponent', () => {
  let spectator: Spectator<HomeComponent>;
  let mockedUiStore: SpyObject<UiStore>;

  const createComponent = createComponentFactory({
    component: HomeComponent,
    imports: [RouterTestingModule],
    providers: [
      mockProvider(UiStore, {
        navItems$: of([])
      }),
      mockProvider(LoadingService, {
        start: null
      })
    ],
    shallow: true
  });

  beforeEach(() => {
    spectator = createComponent();
    mockedUiStore = spectator.inject(UiStore);
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
    // expect(spectator.component.navItems$).toBe(mockedUiStore.navItems$)
  });
});
