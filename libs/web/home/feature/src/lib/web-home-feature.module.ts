import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { HomeComponent } from './home.component'
import { SharedUiPageModule } from '@simulator/shared/ui/page'
import { SharedUiTilesModule } from '@simulator/shared/ui/tiles'
import { ReactiveFormsModule } from '@angular/forms'
import { WebHomeUiResultSimulatorModule } from '@simulator/web/home/ui/result-simulator'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { SharedUiAlertModule } from '@simulator/shared/ui/alert'
import { WebHomeUiMarketIndicatorModule } from '@simulator/web/home/ui/market-indicator'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
      },
    ]),
    SharedUiPageModule,
    SharedUiTilesModule,
    SharedUiAlertModule,
    WebHomeUiMarketIndicatorModule,
    ReactiveFormsModule,
    WebHomeUiResultSimulatorModule,
    NgxChartsModule,
  ],
  declarations: [HomeComponent],
})
export class WebHomeFeatureModule {}
